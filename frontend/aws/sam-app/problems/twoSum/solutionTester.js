const assert = require("assert")
const officialSolution = require("./officialSolution");
const userSolution = require("./userSolution");

const testcases = JSON.parse(process.argv);
for (let i = 0; i < testcases.length; i++) {
    const testcase = testcases[i];
    console.log(testcase);
    const officialResult = officialSolution(...testcase);
    const userResult = userSolution(...testcase);

    if(officialResult != userResult){
        throw new assert.AssertionError({
            message: `outputMismatch,${JSON.stringify(testcase[0])},${testcase[1]}`,
            actual: `${JSON.stringify(userResult)}`,
            expected: `${JSON.stringify(officialResult)}`
        })
    }
}

