const express = require('express');
const cors = require('cors');
const path = require("path");
const { exec, spawn } = require('promisify-child-process');
const { writeFile } = require("fs/promises");
const { body, validationResult } = require("express-validator");
const { v4: uuidv4 } = require('uuid');

const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
app.use(express.json());

app.post('/runCode', [
    body("officialSolutionText").exists({
        checkNull: true
    }),
    body("userSolutionText").exists({
        checkNull: true
    }),
    body("solutionTesterText").exists({
        checkNull: true
    }),
    body("memoryLimitMb").exists({
        checkNull: true
    })
    .isNumeric(),
    body("timeLimitMs").exists({
        checkNull: true
    })
    .isNumeric(),
    body("testCases").exists({
        checkNull: true
    })
], async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ 
            status: "rejected",
            errors: errors.array() 
        });
    }
    console.log("reqGGG");
    console.log(req);
    const officialSolutionText = req.body.officialSolutionText;
    const userSolutionText = req.body.userSolutionText;
    const solutionTesterText = req.body.solutionTesterText;;
    const memoryLimit = req.body.memoryLimitMb;
    const timeLimit = req.body.timeLimitMs;
    const testCases = req.body.testCases;
    const uniqueId = uuidv4();
    
    const officialSolutionJsPath = path.resolve("/tmp", "officialSolution.js");
    const userSolutionJsPath = path.resolve("/tmp", "userSolution.js");
    const solutionTesterJsPath = path.resolve("/tmp", "solutionTester.js");
    const testCasePath = path.resolve("/tmp", "testCase.txt");

    console.log(officialSolutionJsPath);
    await writeFile(officialSolutionJsPath, officialSolutionText);
    await writeFile(userSolutionJsPath, userSolutionText);
    await writeFile(solutionTesterJsPath, solutionTesterText);
    await writeFile(testCasePath, testCases);

    argsRun = [`--max-old-space-size=${memoryLimit}`, solutionTesterJsPath, testCases]
    options = {
        encoding: 'utf8',
        timeout: timeLimit
          // cwd: "/tmp"
    }
    command = `node ${argsRun.join(" ")}`;
    try {
        command = `node ${argsRun.join(" ")}`;
        const { stdout, stderr } = await exec(command, options);
        console.log('stdout', stdout);
        console.log("gay");
        console.log('stderr', stderr);
        stderrArr = stderr.split("\n");
        lastFourLines = stderrArr.slice(stderrArr.length - 4, stderrArr.length);
        message = lastFourLines.reduce((acc, line) => {
            const lineArr = line.split(":");
            if(lineArr != [] && lineArr.length > 2 && lineArr[0] == uniqueId){
              return {
                ...acc,
                [lineArr[1]]: lineArr[2]
              };
            } 
            return acc;
        }, {});
        if(Object.keys(message).length != 0){
            return res.status(200).json({
                status: "assertion error",
                message: message,
                stdout: stdout
            });
        }
    } catch (e) {
        console.log("ok");
        console.log(e);
    }

    return res.status(200).json({
        status: "passed",
        // stats: stats
    });
    //clean up
});

module.exports = app;