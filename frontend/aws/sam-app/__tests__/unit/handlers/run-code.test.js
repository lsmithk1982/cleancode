const lambda = require('../../../src/handlers/run-code'); 

describe('Test Run Code Endpoint', () => {
    it('Should Pass', async () => {
        eventInput = {
            "officialSolutionText": "",
            "userSolutionText": "console.log(\"haha\")",
            "solutionTesterText": "console.log('haha')",
            "memoryLimitMb": 1000,
            "timeLimitMs": 1000,
            "testCases": ""
        }
        
        const data = await lambda.runCodeHandler(eventInput);
        console.log(data);


        // await lambda.runCodeHandler(event);
    })
})