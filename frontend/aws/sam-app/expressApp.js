const express = require('express');
const cors = require('cors');
const { NodeVM, VMScript, } = require('vm2');
const util = require('util');
const deepEqual = require('deep-equal');
const consoleNewLog = require("./consoleNewLog");
const { constructArguments, convertObjToJson, jsonAddText, officialSolutions } = require("./algorithms");

const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
app.use(express.json());

app.post('/runCode', async (req, res) => {
    const codeInput = req.body.codeInput;
    const testCases = req.body.testCases;
    const functionTitle = req.body.functionTitle;
    console.log("heregggggggg");

    if(!codeInput || !testCases || !functionTitle ){
        return res.status(402).json({
            error: "missing argument in body"
        })
    };
    let consoleOutputArr = [];
    const sandbox = {
        console: {
            log: function(...args) { consoleOutputArr.push(consoleNewLog(...args))}
        }
    };
    const nodeVm = new NodeVM({
        console: 'redirect',
        sandbox: sandbox,
        require: {
        builtin: ['fs', 'path'],
        root: "./",
        mock: {
            fs: {
                readFileSync() { return 'Nice try!'; }
            },
            process: {
                cwd() { return 'Nice Try!'; }
            }
        }
        }
    });
    codeInputWithExport = codeInput + "\n" + `module.exports = ${functionTitle};`;
    const script = new VMScript(codeInputWithExport, `solution.js`);
    console.log("gay ggg", testCases);
    console.log("length", testCases.length)
    for(let i = 0; i < testCases.length; i++){
        const testCase = testCases[i];
        console.log("testCases", testCases);
        const arguments = constructArguments(testCase)
        console.log(arguments);
        const userTypedFunction = nodeVm.run(script);
        const officialFunction = officialSolutions[functionTitle]; // change to use problem id
        let userAns;
        try {

            userAns = userTypedFunction(...arguments);
            console.log("userAns", userAns);
        }
        catch(error) {
            console.log("here");
            console.log("gay");
            console.log(error);
            return res.status(200).json({
                status: "runTimeError",
                stderr: error.stack,
                ggg: error.message
            })
        }
        const officialAns = officialFunction(...arguments); //here
        const result = deepEqual(userAns, officialAns);
        if(!result){
            return res.status(200).json({
            status: "assertError",
            lastWrongInput: testCase ? 
                            testCase.map(jsonAddText): 
                            testCase,
            lastWrongOutput: convertObjToJson(userAns),
            lastWrongExpectedOutput: convertObjToJson(officialAns),
            stdout: consoleOutputArr.join("")
            })
        }
        consoleOutputArr = [];
    }
    return res.status(200).json({
        status: "passed"
    })
});

module.exports = app;