export const createBinaryTreeDot = (nodesArray) => {
    const edges = [];
    const visited = {};
    for(let i = 0; i < nodesArray.length; i++){
        console.log("i", i);
        if(nodesArray[i] == null){
            if(i*2+1 < nodesArray.length){
                nodesArray.splice(i*2+1, 0, null);
                console.log("nodesArray", nodesArray);
             }
            if(i*2+2 < nodesArray.length){
                nodesArray.splice(i*2+2, 0, null);
                console.log("nodesArray1", nodesArray);
            }
            continue;
        }
        edges.push(`${i}[label=${nodesArray[i]}]`);
        if(i*2+1 < nodesArray.length){
            console.log("here");
            const leftChild = nodesArray[i*2+1];
            console.log("leftchild", leftChild);
            if(leftChild != null){
              console.log(i*2+1, leftChild);
              edges.push(`${i*2+1}[label=${nodesArray[i+2+1]}]`);  
              edges.push(`${i} -> ${i*2+1};`);
            }
            else {
                edges.push(`null_${i*2+1} [style = invis]; ${i} -> null_${i*2+1} [style = invis];`)
            }
            console.log("edges", edges);
        }
        if(i*2+2 < nodesArray.length){
            const rightChild = nodesArray[i*2+2];
            if(rightChild != null){
                edges.push(`${i*2+2}[label=${nodesArray[i+2+2]}]`);
                visited[i*2+2] = true;
                edges.push(`${i} -> ${i*2+2};`)
            }
            else {
                edges.push(`null_${i*2+2} [style = invis]; ${i} -> null_${i*2+2} [style = invis];`)
            }
        } else {
            edges.push(`null_${i*2+2} [style = invis]; ${i} -> null_${i*2+2} [style = invis];`)
        }
    }
    const str = edges.join("");
    console.log("binarytreeedges", edges);
    console.log("binarytreedot", str);
    const digraph = `digraph G{
        graph [ordering="out"];
        ${str}
      }`;

    return digraph;
}

export const createArrayDot = (array) => {
    const str = array.join("|");
    const digraph = `digraph{ 
        node [shape=record];
        array [label="${str}"];
    }`
    console.log("arraydot", str);

    return digraph;
}

export const createLinkedListDot = (linkedListObj) => {
    let str = "";
    for (const [key, value] of Object.entries(linkedListObj)) {
        const edge = `${key} -> ${value};`;
        str += edge;
    }
    const digraph = `digraph{ 
        rankdir = "LR"
        ${str}
    }`
    console.log("linkedListDot", str)

    return digraph;
}

const createStackDot = (array) => {

}

const createQueueDot = (array) => {

}

const createHashTableDot = (array) => {

}

const createGraph = (array) => {

}

const create2dArray = (array) => {

}