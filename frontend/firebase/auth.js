import firebase from "./init";

export const auth = firebase.auth();

const googleProvider = new firebase.auth.GoogleAuthProvider();
googleProvider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithRedirect(googleProvider).then((result) => console.log("success"));

const facebookProvider = new firebase.auth.FacebookAuthProvider();
// facebookProvider.setCustomParameters({
//   'display': 'popup'
// });
facebookProvider.addScope('user_birthday,email');
export const signInWithFacebook = () => auth.signInWithRedirect(facebookProvider).then((result) => console.log("succes")).catch((error) => console.log(error));
export const signUpWithEmailPassword = async (email, password, userName) => {
  const createdUser = await auth.createUserWithEmailAndPassword(email, password);
  await createdUser.user.updateProfile({
    displayName: userName
  })
  return createdUser;
};
export const sendResetEmail = async (email) => await auth.sendPasswordResetEmail(email);
export const sendVerificationEmail = async () => {
  await auth.currentUser.sendEmailVerification();
}
export const signInWithEmailPassword = async (email, password) => {
  await auth.signInWithEmailAndPassword(email, password);
};

export default firebase;