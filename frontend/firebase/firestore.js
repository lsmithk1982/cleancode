import firebase from "./init";

export const db = firebase.firestore();

export const createUsertoFireStore = async (userAuth) => {
  let userRef;
  try {
    console.log("userAuthfromcreate", userAuth);
    userRef = db.doc(`users/${userAuth.uid}`);
    const userSnapShot = await userRef.get();
    console.log("userSnapShot", userSnapShot);
    if(!userSnapShot.exists) {
      await userRef.set({
        uid: userAuth.uid,
        alerts: 0
      });
    }
  } catch(error) {
    console.log(error);
  }
  return userRef;
};

export const getUserFromFirestore = async (user, callback) => {
  let userData;
  if(!user){
    return userData;
  }
  try {
    const snapShot = await db.doc(`users/${user.uid}`).get()
    userData = snapShot.data();
  } catch(error) {
    console.log(error);
    callback && callback(error);
  }
  return userData;
}