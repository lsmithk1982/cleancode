// Firebase App (the core Firebase SDK) is always required and
// must be listed before other Firebase SDKs
import * as firebase from "firebase/app";

// Add the Firebase services that you want to use
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyC8bL_2rnnFxvg1ePYx74MbP1Q4DDkjPLI",
  authDomain: "interviewprep-cb21d.firebaseapp.com",
  databaseURL: "https://interviewprep-cb21d.firebaseio.com",
  projectId: "interviewprep-cb21d",
  storageBucket: "interviewprep-cb21d.appspot.com",
  messagingSenderId: "811277008116",
  appId: "1:811277008116:web:def92090eec72349ca72a1",
  measurementId: "G-Q3JWJJ32WW"
};

// Initialize Firebase
if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
};

export default firebase;