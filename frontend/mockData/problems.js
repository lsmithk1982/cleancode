const jsInitialCode =`/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function(nums, target) {
    
};`;

const pythonInitialCode = `class Solution(object):
    def twoSum(self, nums, target):
        """
        :type nums: List[int]
        :type target: int
        :rtype: List[int]
        """`;

const javaInitialCode = `class Solution {
    public int[] twoSum(int[] nums, int target) {
        
    }
}`;

const reorderDataInLogFilesSolution1 = {
  title: "Two-pass Hash Table",
  paragraphs: ["To improve our run time complexity, we need a more efficient way to check if the complement exists in the array. If the complement exists, we need to look up its index. What is the best way to maintain a mapping of each element in the array to its index? A hash table.",
  "We reduce the look up time from O(n)O(n) to O(1)O(1) by trading space for speed. A hash table is built exactly for this purpose, it supports fast look up in near constant time. I say 'near' because if a collision occurred, a look up could degenerate to O(n)O(n) time. But look up in hash table should be amortized O(1)O(1) time as long as the hash function was chosen carefully.",
  "A simple implementation uses two iterations. In the first iteration, we add each element's value and its index to the table. Then, in the second iteration we check if each element's complement (target - nums[i]target−nums[i]) exists in the table. Beware that the complement must not be nums[i]nums[i] itself!"],
  codeSol: {"text/x-java": `public int[] twoSum(int[] nums, int target) {
    Map<Integer, Integer> map = new HashMap<>();
    for (int i = 0; i < nums.length; i++) {
        map.put(nums[i], i);
    }
    for (int i = 0; i < nums.length; i++) {
        int complement = target - nums[i];
        if (map.containsKey(complement) && map.get(complement) != i) {
            return new int[] { i, map.get(complement) };
        }
    }
    throw new IllegalArgumentException("No two sum solution");
}`, "javascript": `var twoSum = function (nums, target) {
    const hashMap = {};
  
    for (let i = 0; i < nums.length; i++) {
      const element = target - nums[i];
  
      if (nums[i] in hashMap) {
        return [hashMap[nums[i]], i];
      }
      hashMap[element] = i;
    }
  };`},
  timeComplexity: "O(n)",
  spaceComplexity: "O(n)"
}

const reorderDataInLogFilesSolution2 = {
  title: "One-pass Hash Table",
  paragraphs: ["It turns out we can do it in one-pass. While we iterate and inserting elements into the table, we also look back to check if current element's complement already exists in the table. If it exists, we have found a solution and return immediately"],
  codeSol: {"text/x-java": `public int[] twoSum(int[] nums, int target) {
    Map<Integer, Integer> map = new HashMap<>();
    for (int i = 0; i < nums.length; i++) {
        int complement = target - nums[i];
        if (map.containsKey(complement)) {
            return new int[] { map.get(complement), i };
        }
        map.put(nums[i], i);
    }
    throw new IllegalArgumentException("No two sum solution");
}`, javascript: `const twoSum = (nums, target) => {
  const map = {};

  for (let i = 0; i < nums.length; i++) {
    const another = target - nums[i];

    if (another in map) {
      return [map[another], i];
    }

    map[nums[i]] = i;
  }

  return null;
};`},
  timeComplexity: "O(n)",
  spaceComplexity: "O(n)"
}

const reorderDataInLogFiles = {
  id: "123",
  initialValues: {
    "javascript": jsInitialCode,
    "python": pythonInitialCode,
    "text/x-java": javaInitialCode
  },
  title: "Two Sum",
  functionTitle: "twoSum",
  difficulty: "Easy",
  freqeuncy: "600",
  paragraphs: ["Given an array of integers, return indices of the two numbers such that they add up to a specific target.", 
  "You may assume that each input would have exactly one solution, and you may not use the same element twice."],
  examples: [`Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].`],
  acceptanceRate: "63%",
  solutionExists: true,
  videoExists: false,
  solutions: [reorderDataInLogFilesSolution2, reorderDataInLogFilesSolution1],
  companies: { 
    Amazon: {
      onlineAssess: 3,
      phoneScreen: 30,
      inPerson: 2
    }, 
    Bloomburg: {
      onlineAssess: 1,
      phoneScreen: 20,
      inPerson: 3
    }, 
    "Goldman Sach": {
      onlineAssess: 1,
      phoneScreen: 3,
      inPerson: 1
    }, 
    "Uber": {
      onlineAssess: 5,
      phoneScreen: 1,
      inPerson: 4
    }
  },
  // publicTestCases: [[{type: "tree", data: [1, 2, 3, 4, 6, 6]}, {type: "array", data: [9,10]}]],
  publicTestCases: [[{type: "array", data: [1, 2, 3, 4, 6]}, {type: "int", data: 9}]],
  testCases: {
    "javascript": [
      {
        codeInput: "twoSum([1, 2, 3, 4, 6], 9);",
        arguments: [{type: "array", data: [1, 2, 3, 4, 6]}, {type: "int", data: 9}]
      }, 
      {
        codeInput: "twoSum([2, 11, 3, 4, 6], 13);",
        arguments: [[2, 11, 3, 4, 6], 13]
      }
    ]
  }
}

module.exports = {
  reorderDataInLogFiles,
}