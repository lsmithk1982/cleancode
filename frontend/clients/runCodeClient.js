import ClientHosts from "./clientHosts";

// export async function evalCode(language, codeInput, problemId, testCases, functionTitle, solution) {
//     const uri = ClientHosts.CODE_EVAL_URL;
//     const response = await fetch(uri, {
//         method: "POST",
//         headers: {
//             Accept: "application/json",
//             "Content-Type": "application/json"
//         },
//         body: JSON.stringify({language, codeInput, problemId, testCases, functionTitle, solution})
//     });
//     console.log("response", response);
//     return response.json();
// }

export async function evalCode(language, codeInput, problemId, testCases, functionTitle) {
    if(language == "javascript"){
        return await evalJsCode(codeInput, functionTitle, testCases)
    }
}

export async function evalJsCode(language, codeInput, problemId, testCases, functionTitle) {
    console.log(codeInput)
    console.log("testCases", testCases)
    const uri = ClientHosts.JS_CODE_EVAL_URL;
    const response = await fetch(uri, {
        method: "POST",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json"
        },
        body: JSON.stringify({codeInput, functionTitle, testCases})
    });
    console.log("response", response);
    return response.json();
}
