const ClientHostsDev = {
    CODE_EVAL_URL: "http://localhost:5001/interviewprep-cb21d/us-central1/evaluateCode",
    JS_CODE_EVAL_URL: "http://127.0.0.1:3000/runCode",
    UDS_INTEROP_URL: "https://udsinterop.document.vpsvc.com/api/vpdoc/prod/",
    UDS_URL: "https://uds.documents.cimpress.io/v3"
};

export default ClientHostsDev;