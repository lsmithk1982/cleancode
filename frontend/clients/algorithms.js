const twoSum = function (nums, target) {
    const hashMap = {};
   
    for (let i = 0; i < nums.length; i++) {
      const element = target - nums[i];
      if (nums[i] in hashMap) {
        return [hashMap[nums[i]], i];
      }
      hashMap[element] = i;
    }
    return [];
}

const inorderTraversal = function(root) {
  const traversed = [];
  function inOrderTraverse(node) {
      if (node.left) {
          inOrderTraverse(node.left);
      }
      traversed.push(node.val);
      if (node.right) {
          inOrderTraverse(node.right);
      }
  }
  inOrderTraverse(root);
  return traversed;
};

const invertTree = function(root) {
  if (!root) {
      return root
  }
 
  let left = invertTree(root.left);
  let right = invertTree(root.right);

  root.left = right;
  root.right = left;
 
  return root
};

class TreeNode 
{
    constructor(val, left, right)
    {
        this.val = val;
        this.left = left;
        this.right = right;
    }

    toString() {
      return `Tree(${TreeNode.toString(this)})`;
    }

    static toString(root) {
      console.log(root);
      const result = [];
      const queue = [root];
      while(queue.length > 0){
        const curr = queue.shift();
        if(curr == null){
          continue;
        }
        queue.push(curr.left);
        queue.push(curr.right);
        result.push(curr.val);
      }
      console.log(result);
      return result
    }
}

class LinkedNode 
{
    constructor(val, next)
    {
        this.val = val;
        this.next = next;
    }
}

const officialSolutions = {
    twoSum: twoSum,
    inorderTraversal: inorderTraversal,
    invertTree: invertTree
}

const constructArguments = (arguments) => {
  const convertedArguments = arguments.map((argument) => {
    if(argument.type == "tree"){
      return createBinaryTreeFromLevel(argument.data, 0)
    }
    else if (argument.type == "array"){
      return argument.data
    }
    else if (argument.type == "set"){
      return set(argument.data)
    }
    else if (argument.type == "hashSet"){
      return argument.data.reduce((acc, curr) => ({
        ...acc,
        curr: true
      }), {})
    }
    else if (argument.type == "linkedList"){
      return createLinkedListFromArray(argument.data, 0);
    }
    else if (argument.type == "int"){
      return argument.data;
    }
  })
 
  return convertedArguments;
}

const createBinaryTreeFromLevel = (nodesArray, i) => {
  console.log("here")
  if(i >= nodesArray.length){
    console.log(i);
    return null;
  }
  const newNode = new TreeNode(nodesArray[i], null, null);
  newNode.left = createBinaryTreeFromLevel(nodesArray, i*2+1);
  newNode.right = createBinaryTreeFromLevel(nodesArray, i*2+2);
  return newNode;
}

const createLinkedListFromArray = (arr, i) => {
  if (i >= arr.length){
    return null
  }
  const newNode = new LinkedNode(arr[i], null);
  newNode.next = createLinkedListFromArray(arr, i+1);

  return newNode;
}

const capitalizeFirstLetter = (string) => {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

const stringifyObject = (obj) => {
  if(ans instanceof Array){
    return JSON.stringify(ans);
  }
  else if(ans instanceof TreeNode){
    return ans.toString();
  }
  else {
    return `${ans}`
  }
}

const stringifyArgument = (argument) => {
  if (argument.type == "array"){
    return stringifyObject(argument.data);
  }
  else if(argument.type == "tree"){
    const tree = createBinaryTreeFromLevel(argument.data);
    return stringifyObject(tree);
  }
  else {
    return stringifyObject(argument.data);
  }
}

// const stringifyArgument = (argument) => {
//   if (argument.type == "array"){
//     return `${argument.data}`
//   }
//   else if (argument.type == "tree"){
//     return `Tree(${argument.data})`
//   }
//   else if (argument.type == "set"){
//     return `(${Array.from(new Set(argument.data)).join(",")})`
//   }
//   else if (argument.type == "linkedList"){
//     return argument.data.join(" -> ");
//   }
//   else if (argument.type == "int"){
//     return argument.data;
//   }
// }

const stringifyAns = (ans) => {
  if(ans instanceof Array){
    if(ans.length == 0){
      return {
        type: "array",
        data: JSON.stringify(ans),
        text: "[]"
      }
      // return "[]"
    }
    return `[${ans.join(",")}]`
  }
  else if (ans instanceof Set){
    console.log("okfadfasdfsdfsafsdfs");
    return `(${Array.from(ans).join(",")})`
  }
  else {
    return `${ans}`;
  }
}

const convertObjToJson = (obj) => {
  if(obj instanceof Array){
    return {
      type: "array",
      data: obj,
      text: JSON.stringify(obj)
    }
  }
  else if (obj instanceof TreeNode){
    return {
      type: "tree",
      data: obj,
      text: obj.tString()
    }
  }
  else if (Number.isInteger(obj)){
    return {
      type: "int",
      data: obj,
      text: obj.toString()
    }
  }
  else if (obj instanceof undefined){
    return {
      type: "undefined",
      data: obj,
      text: "undefined"
    }
  }
}

const jsonAddText = (json) => {
  if(json.type == "array"){
    return {
      ...json,
      text: JSON.stringify(json.data)
    }
  }
  else if(json.type == "tree"){
    return {
      ...json,
      text: createBinaryTreeFromLevel(json.data).toString()
    }
  }
  else if(json.type == "int"){
    return {
      ...json,
      text: `${json.data}`
    }
  }
  else if(json.type == "undefined"){
    return {
      ...json,
      text: JSON.stringify(json.data)
    }
  }
}

module.exports = {
  officialSolutions,
  constructArguments,
  TreeNode,
  convertObjToJson,
  jsonAddText
}