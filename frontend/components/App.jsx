import { ThemeProvider } from '@material-ui/core/styles';
import theme from "./ui/Theme";
import Header from "./Header";
import Landing from "./Landing";
import UserProvider from "../components/providers/UserProvider";

function App() {
  return (
    <ThemeProvider theme={theme}>
      <UserProvider>
        <Landing />
      </UserProvider>
    </ThemeProvider>
  );
}

export default App