import {
  Grid,
  Box,
  InputLabel,
  MenuItem,
  FormHelperText,
  FormControl,
  Select,
  makeStyles,
  IconButton,
  Tabs,
  Tab,
  Typography,
  TextField,
  Button,
  Divider,
  Accordion,
  AccordionSummary,
  AccordionDetails
} from "@material-ui/core";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { HorizontalBar } from 'react-chartjs-2';

const styles = (theme) => ({
  exampleBlock: {
    backgroundColor: "#f7f9fa",
    padding: theme.spacing(2) 
  }
});

const useStyle = makeStyles(styles);

function problemDescription({ problem }) {
  const classes = useStyle();
  console.log("problems", problem);
  console.log("companies", problem.companies);
  const companies = Object.keys(problem.companies);
  const companiesData = {
  labels: companies,
  datasets: [
      {
        label: 'Online Assessment',
        backgroundColor: 'rgba(0,0,0,1)',
        borderColor: 'rgba(0,0,0,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(0,0,0,0.5)',
        hoverBorderColor: 'rgba(0,0,0,0.5)',
        data: companies.map((label) => { return problem.companies[label]["onlineAssess"] })
      },
      {
        label: 'Phone Screen',
        backgroundColor: 'rgba(191, 191, 191, 1)',
        borderColor: 'rgba(191, 191, 191, 1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(191,191,191,0.5)',
        hoverBorderColor: 'rgba(191,191,191,0.5)',
        data: companies.map((label) => { return problem.companies[label]["phoneScreen"] })
      },
      {
        label: 'In Person Interview',
        backgroundColor: 'rgba(149, 165, 166, 1)',
        borderColor: 'rgba(149, 165, 166, 1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(149, 165, 166, 0.5)',
        hoverBorderColor: 'rgba(149, 165, 166, 0.5)',
        data: companies.map((label) => { return problem.companies[label]["inPerson"] })
      }
    ]
  };
  const companiesPlotOption = { 
    scales: {
      xAxes: [{ticks: {beginAtZero: true}}],
      yAxes: [{stacked: false}]
    }, 
    responsive: true
  }  
  return (
    <Box>
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Typography variant="h2">
            {problem.title}
          </Typography>
        </Grid>
        <Grid item>
          <Grid container spacing={2}>
            <Grid item>
              <Typography display="inline" variant="subtitle2">
                {`Difficulty: ${problem.difficulty}`}
              </Typography>
            </Grid>
            <Grid item>
              <Typography display="inline" variant="subtitle2">
                {`Seen in interviews: ${problem.freqeuncy}`}
              </Typography>
            </Grid>
          </Grid>
          
        </Grid>
        <Grid item>
          <Divider variant="fullWidth"/>
        </Grid>
        <Grid item>
          {problem.paragraphs.map((paragraph, i) =>
            <Typography key={i} variant="body1" paragraph>{paragraph}</Typography> 
          )}
        </Grid>
        <Grid item>
          <Typography variant="h3"><strong>Examples</strong></Typography>
        </Grid>
        <Grid item>
          {problem.examples.map((example, i) =>
            <Typography key={i} component="pre" className={classes.exampleBlock}>
              {example}
            </Typography> 
            // <pre key={i} className={classes.exampleBlock}>{example}</pre>
          )}
        </Grid>
        <Grid item>
          <Typography>
            {`Acceptance Rate: ${problem.acceptanceRate} `}
          </Typography>
        </Grid>
        <Grid item>
          <Divider variant="fullWidth"/>
        </Grid>
        <Grid item>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="companies-content"
              id="companies-header"
            >
              <Typography>Companies</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <HorizontalBar options={companiesPlotOption} data={companiesData} />
            </AccordionDetails>
          </Accordion>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="techniques-content"
              id="techniques-header"
            >
              <Typography>Techniques and Concepts Involved</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
                sit amet blandit leo lobortis eget.
              </Typography>
            </AccordionDetails>
          </Accordion>
          <Accordion>
            <AccordionSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="similar-problems-content"
              id="similar-problems-header"
            >
              <Typography>Similar Problems</Typography>
            </AccordionSummary>
            <AccordionDetails>
              <Typography>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada lacus ex,
                sit amet blandit leo lobortis eget.
              </Typography>
            </AccordionDetails>
          </Accordion>
        </Grid>
      </Grid>
    </Box>
  )
}

export default problemDescription;