import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputAdornment from '@material-ui/core/InputAdornment';
import EmailIcon from '@material-ui/icons/Email';
import LockIcon from '@material-ui/icons/Lock';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import Link from '@material-ui/core/Link';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import GoogleIcon from './GoogleIcon';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import { Alert } from '@material-ui/lab';
import { 
  signInWithGoogle, 
  signInWithFacebook,
  signInWithEmailPassword
} from '../firebase/auth';
import clsx from 'clsx';


const useStyles = makeStyles((theme) => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  signUpButton: {
    padding: theme.spacing(3)
  },
  forgotPasswordButton: {
    "margin-bottom": theme.spacing(3)
  },
  signUpButton: {
    "margin-bottom": theme.spacing(3)
  },
  button: {
    borderColor: "black",
    color: "black"
  }
}));

function LoginModal({ setOpen, open, className=0 ,setSignUpOpen, handleOpenForgotPassword, ...rest }) {
  const classes = useStyles();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('sm'));
  const [error, setError] = React.useState("");
  const [values, setValues] = React.useState({
    email: '',
    password: '',
    showPassword: false,
  });

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleSignIn = async () => {
    try {
      await signInWithEmailPassword(values.email, values.password);
    } catch(error) {
      console.log(error);
      setError(error.message)
    }
  }

  const handlePressEnterKey = async (event) => {
    if(event.key == "Enter"){
      await handleSignIn();
    }
  }

  return (
    <>
      <Button variant="outlined" className={clsx(classes.button, className)} onClick={handleClickOpen} {...rest}>
        {" Log In "}
      </Button>
      <Dialog maxWidth="xs" open={open} onClose={handleClose} aria-labelledby="Sign Up-dialog">
        <DialogTitle id="form-dialog-title">
          Log In to Your Audiolize Accout!
          <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          <Grid container direction="column" spacing={2}>
            <Grid item>
              <Button
                variant="outlined"
                startIcon={<GoogleIcon />}
                fullWidth
                onClick={signInWithGoogle}
              >
                Login With Google
              </Button>
            </Grid>
            <Grid item>
              <Button
                variant="outlined"
                startIcon={<FacebookIcon htmlColor="rgb(17, 82, 147)"/>}
                fullWidth
                onClick={signInWithFacebook}
              >
                Login With Facebook
              </Button>
            </Grid>
            <Grid item>
              <Button
                variant="outlined"
                startIcon={<TwitterIcon htmlColor="rgb(30, 161, 241)"/>}
                fullWidth
                onClick={signInWithGoogle}
              >
                Login With Twitter
              </Button>
            </Grid>
          </Grid>
          {error &&
          <Box mt={2}>
            <Alert severity="error">
              {/* <AlertTitle>Error</AlertTitle> */}
              {error}
            </Alert>
          </Box>
          }
          <TextField
            autoFocus
            margin="normal"
            id="emailText"
            label="Email Address"
            type="email"
            value={values.email}
            onChange={handleChange('email')}
            variant="outlined"
            fullWidth
            placeholder="Email"
            onKeyPress={handlePressEnterKey}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <EmailIcon />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            id="password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            label="Password"
            variant="outlined"
            margin="normal"
            fullWidth
            placeholder="Password"
            onKeyPress={handlePressEnterKey}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <LockIcon />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                  >
                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </DialogContent>
        <DialogActions className={classes.signUpButton}>
          <Button 
            fullWidth 
            variant="contained" 
            onClick={handleSignIn} 
            fullWidth 
            color="primary"
          >
            Log In
          </Button>
        </DialogActions>
        <Link
            className={classes.forgotPasswordButton}
            component="button"
            variant="body2"
            onClick={handleOpenForgotPassword}
          >
            Forgot Password
        </Link>
        <Box alignSelf="center">
          <span style={{verticalAlign: "top"}}>
            {"Don't have an account? "}
          </span>
          <Link
              className={classes.signUpButton}
              component="button"
              variant="body2"
              onClick={()=>{setOpen(false); setSignUpOpen(true)}}
            >
             Sign up
          </Link>
        </Box>
      </Dialog>
    </>
  )
}

export default LoginModal;