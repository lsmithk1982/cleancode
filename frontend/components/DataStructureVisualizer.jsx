import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    makeStyles,
    IconButton,
    Button,
    Typography,
    Box,
    Grid,
    TextField,
    Tooltip
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import Dot from "./Dot";


const styles = (theme) => ({
    closeButton: {
        position: 'absolute',
        right: theme.spacing(1),
        top: theme.spacing(1),
        color: theme.palette.grey[500],
    },
    dialogActions: {
        paddingLeft: theme.spacing(3),
        paddingRight: theme.spacing(3)
    },
    textFieldRoot: {
        borderRadius: 20
    }
});

const useStyles = makeStyles(styles);

function DataStructureVisualizer({ 
    open=false, 
    handleClose, 
    handleConfirm, 
    argus, 
    handleReset, 
    onTextFieldChange, 
    onTextFieldFocus, 
    currentDot,
    currentDataType,
    allowEditing=false
}){
    const classes = useStyles();
    
    // const [currentDot, setCurrentDot] = React.useState(null);
    // const [argus, setArgus] = React.useState(args.map((arg) => ({
    //     type: arg.type,
    //     value: JSON.stringify(arg.data)
    // })));
    // const [currentDataType, setCurrentDataType] = React.useState(null);

    // const onTextFieldChange = (index, event) => {
    //     if(argus[index].type == "tree"){
    //         try {
    //             const nodesArray = JSON.parse(event.target.value);
    //             const nextDot = createBinaryTreeDot(nodesArray);
    //             setCurrentDot(nextDot);
    //             const nextState = [...argus];
    //             nextState[index] = { ...nextState[index], value: event.target.value }
    //             setArgus(nextState);
    //         } catch {
    //             const nextState = [...argus];
    //             nextState[index] = { ...nextState[index], value: event.target.value }
    //             setArgus(nextState);
    //         }
    //     }
    //     else if(argus[index].type == "linkedList") {
    //         try {
    //             const linkedListObj = JSON.parse(event.target.value);
    //             const nextDot = createLinkedListDot(linkedListObj);
    //             setCurrentDot(nextDot);
    //             const nextState = [...argus];
    //             nextState[index] = { ...nextState[index], value: event.target.value }
    //             setArgus(nextState);
    //         } catch {
    //             const nextState = [...argus];
    //             nextState[index] = { ...nextState[index], value: event.target.value }
    //             setArgus(nextState);
    //         }
    //     }
    //     else if(argus[index].type == "array") {
    //         try {
    //             const array = JSON.parse(event.target.value);
    //             const nextDot = createArrayDot(array);
    //             setCurrentDot(nextDot);
    //             const nextState = [...argus];
    //             nextState[index] = { ...nextState[index], value: event.target.value }
    //             setArgus(nextState);
    //         } catch {
    //             const nextState = [...argus];
    //             nextState[index] = { ...nextState[index], value: event.target.value }
    //             setArgus(nextState);
    //         }
    //     }
    // };

    // const onTextFieldFocus = (index, event) => {
    //     if(argus[index].type == "tree"){
    //         try {
    //             const nodesArray = JSON.parse(event.target.value);
    //             console.log("nodesArray", nodesArray);
    //             const nextDot = createBinaryTreeDot(nodesArray);
    //             setCurrentDataType("tree");
    //             console.log("binary tree");
    //             console.log("nextDotb", nextDot);
    //             setCurrentDot(nextDot);
    //         } catch(e) {
    //             console.log(e);
    //             setCurrentDataType("tree");
    //             setCurrentDot("");
    //         }
    //     } 
    //     else if (argus[index].type == "linkedList"){
    //         try {
    //             const linkedListObj = JSON.parse(event.target.value);
    //             console.log("link", linkedListObj);
    //             const nextDot = createLinkedListDot(linkedListObj);
    //             setCurrentDataType("linkedList");
    //             console.log("nextDotl", nextDot);
    //             setCurrentDot(nextDot);
    //         } catch(e) {
    //             console.log(e);
    //             setCurrentDataType("linkedList");
    //             setCurrentDot("");
    //         }
    //     }
    //     else if (argus[index].type == "array"){
    //         try {
    //             const array = JSON.parse(event.target.value);
    //             console.log("array", array);
    //             const nextDot = createArrayDot(array);
    //             setCurrentDataType("array");
    //             console.log("nextDota", nextDot);
    //             setCurrentDot(nextDot);
    //         } catch(e) {
    //             console.log(e);
    //             setCurrentDataType("array");
    //             setCurrentDot("");
    //         }
    //     }
    // }
    console.log(currentDataType, "datatype");
    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="edit-problem-dialog-title"
            aria-describedby="edit-problem-dialog-description"
            maxWidth="xs"
            fullWidth
        >
            <DialogTitle disableTypography>
                <Typography variant="h4">
                {"Custom Test Case"}
                </Typography>
                <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent dividers>
                {(() => {
                    if(currentDataType == "tree" || currentDataType == "linkedList" || currentDataType == "array"){
                        console.log("datatype");
                        return (
                            <Box height={300}>
                                <Dot dot={currentDot}/>
                            </Box>
                        )
                    } else {
                        return null
                    }
                })()}
            </DialogContent>
            <DialogContent dividers>
                <Grid container direction="column" spacing={1}>
                {argus.map((argu, index) => {
                    console.log("argus", argus);
                    return (
                    <Grid item key={index}>
                        <Grid container alignItems="center" spacing={1}>
                        <Grid item>
                            {`Argument ${index+1} (${argu.type}): `}
                        </Grid>
                        <Grid item style={{ flex: 1, minWidth: 0 }}>
                            <Tooltip 
                                disableFocusListener 
                                disableTouchListener 
                                disableHoverListener={allowEditing} 
                                title={<Typography>currently we don't support editing test, but you can click the textfield and view visualization</Typography>}
                                placement="bottom"
                            >
                                <TextField 
                                    fullWidth
                                    value={argu.value}
                                    onChange={onTextFieldChange.bind({}, index)}
                                    onFocus={onTextFieldFocus.bind({}, index)}
                                    autoFocus={index == 0}
                                    variant="outlined"
                                    classes={{root: classes.textFieldRoot}}
                                    onClick={() => {console.log("okayy")}}
                                />
                            </Tooltip>
                        </Grid>
                        </Grid>
                    </Grid>
                    )
                })}
                </Grid>
            </DialogContent>
            { allowEditing &&
                <DialogActions className={classes.dialogActions}>
                    <Button 
                        variant="outlined" 
                        onClick={handleReset}
                        color="primary"
                    >
                        {"Reset to Default"}
                    </Button>
                    <Button 
                        variant="contained" 
                        onClick={handleConfirm}  
                        color="primary"
                    >
                        {"Save & Continue"}
                    </Button>
                </DialogActions>
            }
        </Dialog>
        )
    }

export default DataStructureVisualizer;