import {
  Grid,
  Box,
  InputLabel,
  MenuItem,
  FormHelperText,
  FormControl,
  Select,
  makeStyles,
  IconButton,
  Tabs,
  Tab,
  Typography,
  TextField,
  Button,
  Divider
} from "@material-ui/core";
import {
  Alert,
  AlertTitle
} from "@material-ui/lab";
import SettingsIcon from '@material-ui/icons/Settings';
import FullscreenIcon from '@material-ui/icons/Fullscreen';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExit';
import RefreshIcon from '@material-ui/icons/Refresh';
import CodeIcon from '@material-ui/icons/Code';
import CloseIcon from '@material-ui/icons/Close';
import CodeEditor from './CodeEditor';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import CenteredPlaceholder from './CenteredPlaceholder';
import Loading from './Loading';
import EditorSettingModal from './EditorSettingModal';
import ConfirmationModal from './ConfirmationModal';
import { evalCode, evalJsCode } from '../clients/runCodeClient';
import DataStructureVisualizer from './DataStructureVisualizer';
import CollapsibleTable from './CollapsibleTable';
import {
  createBinaryTreeDot,
  createArrayDot,
  createLinkedListDot
} from "../utils/graphvizUtils"

const styles = (theme) => ({
  formControl: {
    minWidth: 120,
    maxHeight: "50px"
  },
  inputLabel: {
    color: "black"
  },
  langaugeDropDown: {
    marginRight: "auto"
  },
  testsInput: {
    backgroundColor: 'white'
  },
  tabs: {
    backgroundColor: '#f7f7f7'
  },
  activeTab: {
    backgroundColor: 'white'
  },
  tabPanel: {
    backgroundColor: "white"
  },
  tabIndictator: {
    backgroundColor: "white"
  },
  consoleButton: {
    marginRight: "auto"
  },
  codeAreaButtons: {
    paddingTop: theme.spacing(2.5),
    paddingBottom: theme.spacing(2.5),
    paddingLeft: theme.spacing(1.25),
    paddingRight: theme.spacing(1.25)
  },
  editorContainer: {
    height: "100%"
  },
  successContainer: {
    height: "100%",
    width: "100%",
    minHeight: "6.5rem",
    border: "3px solid #00af32",
    color: "#00af32",
    padding: theme.spacing(2)
  },
  checkCircleIcon: {
    fontSize: "3.5rem"
  },
  runTimeErrorContainer: {
    // backgroundColor: "rgb(252, 228, 236)",
    color: "rgb(233, 30, 99)",
    // paddingLeft: theme.spacing(1),
    // paddingRight: theme.spacing(1),
    overflowX: "auto",
    whiteSpace: "pre-wrap",
    whiteSpace: "-moz-pre-wrap",
    whiteSpace: "-pre-wrap",
    whiteSpace: "-o-pre-wrap",
    wordWrap: "break-word"
  },
  tabCloseButton: {
    marginLeft: "auto"
  },
  muiTabWrapper: {
    alignItems: "flex-end"
  },
  localTestCaseContainer: {
    height: "100%",
    width: "100%",
    minHeight: "4rem"
  },
  testsButton: {
    marginRight: "auto"
  },
  runPublicTestButton: {
    borderColor: "black",
    color: "black"
  }
});

const useStyles = makeStyles(styles);

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={1}>
          {children}
        </Box>
      )}
    </div>
  );
}

function AssertEqualErrorPanel({ problemInput=[], expectedOutput="", userOutput="", stdout="" }) {
  console.log("problemInput", problemInput);
  console.log("userOutput1  ", userOutput);
  const stdoutWithPrefix = stdout.split("\n").map((line) => { return `> ${line}` }).join("\n");
  const [problemGridHeight, setProblemGridHeight] = React.useState("auto");
  const problemGridEl = React.useRef(null);
  const terminalGridEl = React.useRef(null);

  React.useLayoutEffect(() => {
    setProblemGridHeight(problemGridEl.current.clientHeight);
  }, [problemGridEl])

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item>
        <Alert severity="error">
          <Typography>
            Assertion Error - Wrong Answer
          </Typography>
        </Alert>
      </Grid>
      <Grid item style={{ paddingBottom: 0, width: "100%" }}>
        <Grid container spacing={2}>
          <Grid item xs={stdout ? 6: 12}>
            <Grid container direction="column" justify="center" spacing={2} ref={problemGridEl}>
              <Grid item style={{width: "100%"}}>
                <Grid container alignItems="center" spacing={2}>
                  <Grid item>
                    <Typography>
                      Problem Input
                    </Typography>
                  </Grid>
                  <Grid item style={{flex: 1, minWidth: 0}}>
                    <Box border={1} p={2} borderRadius={"20px"} style={{backgroundColor: "#EDF5FD", overflowX: "auto"}}>
                      <Typography component="pre">
                        {problemInput.map(el => el.text).join('\n')}
                      </Typography>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item style={{width: "100%"}}>
                <Grid container alignItems="center" spacing={2}>
                  <Grid item>
                    <Typography>
                      Expected Output
                    </Typography>
                  </Grid>
                  <Grid item style={{flex: 1, minWidth: 0}}>
                    <Box border={1} p={2} borderRadius={"20px"} style={{backgroundColor: "#EDF5FD", overflowX: "auto"}}>
                      <Typography>
                        {expectedOutput ? expectedOutput.text : "undefined"}
                      </Typography>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item style={{width: "100%"}}>
                <Grid container alignItems="center" spacing={2}>
                  <Grid item>
                    <Typography>
                      Your Output
                    </Typography>
                  </Grid>
                  <Grid item style={{flex: 1, minWidth: 0}}>
                    <Box borderColor="red" border={2} p={2} borderRadius={"20px"} style={{backgroundColor: "rgb(253, 236, 234)"}}>
                      <Typography noWrap={true}>
                        {userOutput ? userOutput.text: "undefined"}
                      </Typography>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          { stdout && 
          <Grid item xs={6} style={problemGridHeight ? { height: problemGridHeight }: {}}>
            <CodeEditor configOptions={{
                theme: "zenburn",
                lineNumbers: false,
                styleActiveLine: false
              }}
              codeInputs={{"shell": stdoutWithPrefix}}
              readOnly
              mode="shell"
            />
          </Grid> }
        </Grid>
      </Grid>
    </Grid>
  )
}

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

const initialEdtiorSetting = {
  fontSize: "16px",
  tabSize: 2,
  theme: "default",
  keyBinding: "default"
};

function CodingPlaygroud({ problem, handleResize, maxSize }) {
  const classes = useStyles();

  const [language, setLanguage] = React.useState("javascript");
  const [codeInputs, setCodeInputs] = React.useState({
    "javascript": problem.initialValues["javascript"],
    "python": problem.initialValues["python"],
    "text/x-java": problem.initialValues["text/x-java"]
  });
  const [tabValue, setTabValue] = React.useState(0);
  const [consolePanel, setConsole] = React.useState(true);
  const [runTestResult, setRunTestResult] = React.useState({
    status: null
  });
  const [settingModal, setSettingModal] = React.useState(false);
  const [editorSettings, setEditorSettings] = React.useState({ ...initialEdtiorSetting  })
  const [refreshConfirmationModal, setRefreshConfirmationModal] = React.useState(false);
  
  // const [localTestCase, setLocalTestCase] = React.useState(problem.localTestCases[0]);
  const [editInputModal, setEditInputModal] = React.useState(false);
  const [currentDot, setCurrentDot] = React.useState(null);
  const [argus, setArgus] = React.useState(problem.publicTestCases[0].map((arg) => ({
    type: arg.type,
    value: JSON.stringify(arg.data)
  })));
  const [currentDataType, setCurrentDataType] = React.useState(null);
  const [savingWarningModal, setSavingWarningModal] = React.useState(false);
  const [localTestAllowEditing, setLocalTestAllowEditing] = React.useState(false);

  const onTextFieldChange = (index, event) => {
      if(!localTestAllowEditing){
        return;
      }
      if(argus[index].type == "tree"){
          try {
              const nodesArray = JSON.parse(event.target.value);
              const nextDot = createBinaryTreeDot(nodesArray);
              setCurrentDot(nextDot);
              const nextState = [...argus];
              nextState[index] = { ...nextState[index], value: event.target.value }
              setArgus(nextState);
          } catch {
              const nextState = [...argus];
              nextState[index] = { ...nextState[index], value: event.target.value }
              setArgus(nextState);
          }
      }
      else if(argus[index].type == "linkedList") {
          try {
              const linkedListObj = JSON.parse(event.target.value);
              const nextDot = createLinkedListDot(linkedListObj);
              setCurrentDot(nextDot);
              const nextState = [...argus];
              nextState[index] = { ...nextState[index], value: event.target.value }
              setArgus(nextState);
          } catch {
              const nextState = [...argus];
              nextState[index] = { ...nextState[index], value: event.target.value }
              setArgus(nextState);
          }
      }
      else if(argus[index].type == "array") {
          try {
              const array = JSON.parse(event.target.value);
              const nextDot = createArrayDot(array);
              setCurrentDot(nextDot);
              const nextState = [...argus];
              nextState[index] = { ...nextState[index], value: event.target.value }
              setArgus(nextState);
          } catch {
              const nextState = [...argus];
              nextState[index] = { ...nextState[index], value: event.target.value }
              setArgus(nextState);
          }
      }
  };

  const onTextFieldFocus = (index, event) => {
    if(argus[index].type == "tree"){
        try {
            const nodesArray = JSON.parse(event.target.value);
            console.log("nodesArray", nodesArray);
            const nextDot = createBinaryTreeDot(nodesArray);
            setCurrentDataType("tree");
            console.log("binary tree");
            console.log("nextDotb", nextDot);
            setCurrentDot(nextDot);
        } catch(e) {
            console.log(e);
            setCurrentDataType("tree");
            setCurrentDot("");
        }
    } 
    else if (argus[index].type == "linkedList"){
        try {
            const linkedListObj = JSON.parse(event.target.value);
            console.log("link", linkedListObj);
            const nextDot = createLinkedListDot(linkedListObj);
            setCurrentDataType("linkedList");
            console.log("nextDotl", nextDot);
            setCurrentDot(nextDot);
        } catch(e) {
            console.log(e);
            setCurrentDataType("linkedList");
            setCurrentDot("");
        }
    }
    else if (argus[index].type == "array"){
        try {
            const array = JSON.parse(event.target.value);
            console.log("array", array);
            const nextDot = createArrayDot(array);
            setCurrentDataType("array");
            console.log("nextDota", nextDot);
            setCurrentDot(nextDot);
        } catch(e) {
            console.log(e);
            setCurrentDataType("array");
            setCurrentDot("");
        }
    } 
    else {
      setCurrentDataType("int");
      setCurrentDot("");
    }
  }

  const handleEditorSettingChange = (id, event) => {
    console.log("id", id);
    // console.log("event", event);
    // console.log("event.currtarget", event.currentTarget);
    // console.log("event.target", event.target);
    if(id == "fontSizeDropdown"){
      setEditorSettings({
        ...editorSettings,
        fontSize: event.target.value
      })
    } 
    else if(id == "tabSizeDropdown"){
      setEditorSettings({
        ...editorSettings,
        tabSize: event.target.value
      })
    }
    else if(id == "themeDropdown"){
      setEditorSettings({
        ...editorSettings,
        theme: event.target.value
      })
    }
    else if(id == "keyBindingDropdown"){
      setEditorSettings({
        ...editorSettings,
        keyBinding: event.target.value
      })
    }
  }

  const handleEditorReset = () => {
    setEditorSettings({
     ...initialEdtiorSetting 
    })
  }

  const handleEditorApply = () => {
    setSettingModal(false);
  }

  const handleChange = (event) => {
    setLanguage(event.target.value);
  };

  const refreshCodeEditor = () => {
    localStorage.removeItem(`${problem.id}_${language}`);
    setCodeInputs({...codeInputs, [language]: problem.initialValues[language]});
    setRefreshConfirmationModal(false);
  }

  const handleTabChange = (event, newValue) => {
    if(newValue != 2){
      setTabValue(newValue);
    } else {
      setConsole(false);
    }
  };

  const handleConsoleToggle = (event, newValue) => {
    setConsole(!consolePanel);
  }

  const handleRunCode = async () => {
    setConsole(true);
    setTabValue(0);
    setRunTestResult({
      status: "loading"
    });
    const data = await evalJsCode(language, codeInputs[language], problem.id, 
                                problem.publicTestCases, problem.functionTitle);
    console.log("data", data);
    setRunTestResult({
      ...data
    });
  };

  const handleSettingModal = (status) => {
    setSettingModal(status);
  }

  const handleDSVSave = () => {
    localStorage.setItem(`${problem.id}_local_testCase`, JSON.stringify(argus));
    setEditInputModal(false);
  }

  const handleDSVReset = () => {
    localStorage.removeItem(`${problem.id}_local_testCase`);
    setLocalTestCase(problem.publicTestCases[0]);
  }

  React.useEffect(() => {
    const data = localStorage.getItem(`${problem.id}_lastLan`);
    const setting_data = JSON.parse(localStorage.getItem(`editor_settings`));
    const local_testCase = JSON.parse(localStorage.getItem(`${problem.id}_local_testCase`));
    if (data) setLanguage(data);
    if (setting_data) setEditorSettings(setting_data);
    if (local_testCase && localTestAllowEditing) setArgus(local_testCase);
  }, []);

  React.useEffect(() => {
    const data = localStorage.getItem(`${problem.id}_${language}`);
    localStorage.setItem(`${problem.id}_lastLan`, language);
    if (data) setCodeInputs({...codeInputs, [language]: data});
  }, [language]);

  React.useEffect(() => {
    localStorage.setItem(`${problem.id}_${language}`, codeInputs[language]);
  }, [codeInputs]);

  React.useEffect(() => {
    localStorage.setItem(`editor_settings`, JSON.stringify(editorSettings));
  }, [editorSettings]);

  return (
    <Box width="100%" height="100%">
      <Grid container direction="column" className={classes.editorContainer}>
        <Grid item>
          <Box width="100%" p={2.5}>
            <Grid container justify="flex-end">
              <Grid item className={classes.langaugeDropDown}>
                <FormControl variant="outlined" color="secondary" className={classes.formControl}>
                  <InputLabel id="code-language-selection-label" className={classes.inputLabel}>Language</InputLabel>
                  <Select
                    labelId="language-selection-label"
                    id="language-select-outlined"
                    value={language}
                    onChange={handleChange}
                    label="Javascript"
                  >
                    <MenuItem value={"javascript"}>Javascript</MenuItem>
                    <MenuItem value={"python"}>Python</MenuItem>
                    <MenuItem value={"text/x-java"}>Java</MenuItem>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item>
                <IconButton onClick={() => setRefreshConfirmationModal(true)}>
                  <RefreshIcon id="refresh-codeEditor"/>
                </IconButton>
                <ConfirmationModal 
                  open={refreshConfirmationModal}
                  handleClose={() => setRefreshConfirmationModal(false)}
                  handleConfirm={refreshCodeEditor}
                  title={"Warning! Please Read Before Proceed"}
                  text={"Your current input in the code editor will be deleted and reset to our default code. You can't undo this operation!"}
                />
              </Grid>
              <Grid item>
                <IconButton>
                  <CodeIcon />
                </IconButton>
              </Grid>
              <Grid item>
                <IconButton onClick={() => handleSettingModal(true)}>
                  <SettingsIcon />
                </IconButton>
                <EditorSettingModal 
                  open={settingModal} 
                  handleClose={() => handleSettingModal(false)}
                  settings={editorSettings}
                  handleChange={handleEditorSettingChange}
                  handleReset={handleEditorReset}
                  handleApply={handleEditorApply}
                />
              </Grid>
              <Grid item>
                <IconButton onClick={handleResize}>
                  {maxSize ? <FullscreenExitIcon /> : <FullscreenIcon />}
                </IconButton>
              </Grid>
            </Grid>
          </Box>
        </Grid>
        <Grid item style={{flex: 1, minHeight: 0, width: "100%"}}>
          <CodeEditor 
            mode={language} 
            codeInputs={codeInputs} 
            setCodeInputs={setCodeInputs} 
            height="100%" 
            fontSize={editorSettings.fontSize}
            configOptions={{
              tabSize: editorSettings.tabSize,
              indentUnit: editorSettings.tabSize,
              keyMap: editorSettings.keyBinding,
              theme: editorSettings.theme
            }}
          />
        </Grid>
        <Grid item style={{backgroundColor: "red", width: "100%"}}>
          {consolePanel && 
            <>
              <Tabs
                value={tabValue}
                onChange={handleTabChange}
                indicatorColor="secondary"
                textColor="inherit"
                aria-label="custom tests"
                className={classes.tabs}
                TabIndicatorProps={{className: classes.tabIndictator}}
              >
                <Tab label="Public Test Result" className={tabValue == 0 ? classes.activeTab : classes.tab} {...a11yProps(0)} />
                <Tab label="Server Tests Result" className={tabValue == 1 ? classes.activeTab : classes.tab} {...a11yProps(1)} />
                <Tab className={classes.tabCloseButton} classes={{wrapper: classes.muiTabWrapper}} icon={<CloseIcon />} {...a11yProps(2)} />
              </Tabs>
              <TabPanel className={classes.tabPanel} value={tabValue} index={0}>
                {(() => {
                  if (runTestResult.status === "loading") {
                    return (
                      <Loading />
                    )
                  }
                  else if (runTestResult.status === "assertError") {
                    return (
                      <Box p={2} pt={1}>
                        <AssertEqualErrorPanel
                          problemInput={runTestResult.lastWrongInput}
                          expectedOutput={runTestResult.lastWrongExpectedOutput}
                          userOutput={runTestResult.lastWrongOutput}
                          stdout={runTestResult.stdout}
                        />
                      </Box>
                    )
                  } 
                  else if (runTestResult.status === "runTimeError") {
                    return (
                      <Alert severity="error" className={classes.runTimeErrorContainer}>
                        {/* <AlertTitle style={{color: "rgb(233, 30, 99)"}}>RunTime Error</AlertTitle> */}
                        <Typography component="pre" className={classes.runTimeErrorContainer}>
                          {runTestResult.stderr}
                        </Typography>
                      </Alert>
                    )
                  }
                  else if (runTestResult.status === "passed") {
                    return (
                      <Grid 
                        container 
                        direction="column" 
                        spacing={1}
                      >
                        <Grid item>
                          <Alert severity="success">
                            Congratulations! You Passed The Public Test!
                          </Alert>
                        </Grid>
                        <Grid item>
                          <CollapsibleTable />
                        </Grid>
                      </Grid>
                    )
                  } 
                  else {
                    return (
                      <Grid container alignItems="center" justify="center">
                        <CenteredPlaceholder text={"You Have to Run Code First"}/>
                      </Grid>
                    )
                  }
                })()} 
              </TabPanel>
              <TabPanel className={classes.tabPanel} value={tabValue} index={1}>
                <CenteredPlaceholder text={"You Have to Submit Code First"}/>
              </TabPanel>
            </>}
            </Grid>
        <Grid item >
          <Grid container justify="flex-end" className={classes.codeAreaButtons} alignItems="center">
            <Grid item className={classes.consoleButton}>
              <Grid container spacing={1}>
                <Grid item>
                  <Button 
                    endIcon={consolePanel ? <ExpandMoreIcon />: <ExpandLessIcon />} 
                    onClick={handleConsoleToggle}>
                    Test Results
                  </Button>
                </Grid>
                <Grid item>
                  <Button variant="outlined" onClick={() => { setEditInputModal(true) }}>
                      View/Edit Public Test
                  </Button>
                  <DataStructureVisualizer 
                    open={editInputModal}
                    handleClose={ localTestAllowEditing ? () => { setSavingWarningModal(true) } : () => { setEditInputModal(false); }}
                    handleConfirm={handleDSVSave}
                    onTextFieldChange={onTextFieldChange}
                    onTextFieldFocus={onTextFieldFocus}
                    currentDataType={currentDataType}
                    currentDot={currentDot}
                    handleReset={handleDSVReset}
                    argus={argus}
                    allowEditing={localTestAllowEditing}
                  />
                  <ConfirmationModal 
                    open={savingWarningModal}
                    handleClose={() => setSavingWarningModal(false)}
                    handleConfirm={() => { setSavingWarningModal(false); setEditInputModal(false); }}
                    title={"Warning! Not Saved Yet"}
                    text={"Your local test hasn't been saved yet, the change will only be reflected in the current session. If you want your change to persist after refreshing go back and click save"}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Box marginRight={2}>
              <Grid item>
                <Button variant="outlined" className={classes.runPublicTestButton} onClick={handleRunCode}>
                  Run Public Test
                </Button>
              </Grid>
            </Box>
            <Grid item>
              <Button variant="contained" color="primary">
                Submit For Server Tests
              </Button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
}

export default CodingPlaygroud;