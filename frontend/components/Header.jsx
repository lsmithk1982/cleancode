import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import SignUpModal from "./SignUpModal";
import LoginModal from "./LoginModal";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Hidden from '@material-ui/core/Hidden';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import ProfileButton from './ProfileButton';
import AccountAlertsButton from './AccountAlertsButton';
import { UserContext } from "../components/providers/UserProvider";
import ForgotPasswordModal from "./ForgotPasswordModal";

const useStyles = makeStyles((theme) => ({
  logo: {
    height: "2.5em",
    width:  "2.5em"
  },
  search: {
    height: "2.5em"
  },
  navBar: {
    paddingLeft: "10%",
    paddingRight: "10%"
  },
  userActions: {
    marginLeft: "auto"
  },
  signupButton: {
    marginLeft: theme.spacing(1)
  },
  tab: {
    minWidth: 10,
    '&:hover': {
      color: "black",
      opacity: 1
    },
    '&:focus': {
      color: "black"
    }
  },
  hamburger: {
    width: "1.5em",
    height: "1.5em"
  },
  loginButtonMobile: {
    border: 0,
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  },
  signupButtonMobile: {
    border: 0,
    paddingTop: theme.spacing(1),
    paddingBottom: theme.spacing(1),
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2)
  }
}));

function Header({ position="fixed" }) {
  const { user } = React.useContext(UserContext);
  const classes = useStyles();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('sm'));
  const [signUpOpen, setSignUpOpen] = React.useState(false);
  const [loginOpen, setLoginOpen] = React.useState(false);
  const [tabValue, setTabValue] = React.useState(0);
  const [drawerOpen, setDrawerOpen] = React.useState(false);
  const [openForgotPassword, setForgotPassword] = React.useState(false);
  const headerItemsLabels = ["What is CleanCode?", "Explore", "Content", "Premium"]
  const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);
  const LoginRef = React.forwardRef(() => (<LoginModal className={classes.loginButtonMobile} setOpen={setLoginOpen} open={loginOpen} setSignUpOpen={setSignUpOpen}/>));
  const SignupRef = React.forwardRef(() => (<SignUpModal variant="outlined" className={classes.signupButtonMobile} setOpen={setSignUpOpen} open={signUpOpen} setLoginOpen={setLoginOpen}/>));
  
  const handleChange = (event, newTabValue) => {
    setTabValue(newTabValue);
  };

  const handleForgotPasswordClose = () => {
    setForgotPassword(false);
  }

  const handleOpenForgotPassword = () => {
    setForgotPassword(true);
    setLoginOpen(false);
  }

  const drawer = (
    <>
      <IconButton
        edge="start"
        className={classes.menuButton}
        color="inherit"
        aria-label="open drawer"
        onClick={() => setDrawerOpen(!drawerOpen)}
      >
        <MenuIcon className={classes.hamburger}/>
      </IconButton>
      <SwipeableDrawer
        anchor="left"
        open={drawerOpen}
        disableBackdropTransition={!iOS} disableDiscovery={iOS}
        onClose={() => setDrawerOpen(false)}
        onOpen={() => setDrawerOpen(true)}
      >
        <List>
          {headerItemsLabels.map((text) => (
            <ListItem button key={text}>
              <ListItemText primary={text} />
            </ListItem>
          ))}
        </List>
        <Divider />
        <List>
          <ListItem component={LoginRef} />
            or
          <ListItem component={SignupRef} />
        </List>
      </SwipeableDrawer>
    </>
  );
  return (
    <div>
      <AppBar position={position} color="secondary">
        <Toolbar disableGutters>
          <Grid justify={matches ? "space-between" : "flex-start"} container alignItems="center" spacing={4} className={classes.navBar}>
            <Hidden mdUp>
              <Grid item>
                {drawer}
              </Grid>
            </Hidden>
            <Grid item>
              <img alt="company logo" src="/headphones.svg" className={classes.logo}/>
            </Grid>
            <Hidden smDown>
              <Grid item>
                <Tabs
                  value={tabValue}
                  onChange={handleChange}
                  aria-label="navigation tabs"
                >
                  <Tab className={classes.tab} label="What is CleanCode?" />
                  <Tab className={classes.tab} label="Explore" />
                  <Tab className={classes.tab} label="Content" />
                  <Tab className={classes.tab} label="Premium" />
                </Tabs>
              </Grid>
              <Grid item className={classes.userActions}>
                {user ? 
                  <Grid container direction="row" spacing={1}>
                    <Grid item>
                      <AccountAlertsButton alertsNum={user.alerts}/>
                    </Grid>
                    <Grid item>
                      <ProfileButton />
                    </Grid>
                  </Grid> :
                  <Grid container direction="row" spacing={2}>
                    <Grid item>
                      <LoginModal 
                        setOpen={setLoginOpen} 
                        open={loginOpen} 
                        setSignUpOpen={setSignUpOpen}
                        handleOpenForgotPassword={handleOpenForgotPassword}
                      />
                    </Grid>
                    <Grid item>
                      <SignUpModal 
                        setOpen={setSignUpOpen} 
                        open={signUpOpen} 
                        setLoginOpen={setLoginOpen}
                      />
                    </Grid>
                  </Grid>
                }
              </Grid>
            </Hidden>
          </Grid>
        </Toolbar>
      </AppBar>
      <ForgotPasswordModal open={openForgotPassword} handleClose={handleForgotPasswordClose}/>
    </div>
  );
}

export default Header;