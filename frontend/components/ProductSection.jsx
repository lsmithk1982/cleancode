import { makeStyles } from "@material-ui/core/styles";
import Chat from "@material-ui/icons/Chat";
import VerifiedUser from "@material-ui/icons/VerifiedUser";
import Fingerprint from "@material-ui/icons/Fingerprint";
import EventNote from "@material-ui/icons/EventNote";
import SettingsSystemDaydreamIcon from '@material-ui/icons/SettingsSystemDaydream';
import ClassIcon from '@material-ui/icons/Class';
import FastForwardIcon from '@material-ui/icons/FastForward';
import CodeIcon from '@material-ui/icons/Code';
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import InfoArea from "./InfoArea";

const styles = {
  productSection: {
    margin: "auto",
    maxWidth: "1080px"
  },
  section: {
    // padding: "70px 0",
    textAlign: "center"
  },
  title: {
    color: "#3C4858",
    margin: "1.75rem 0 0.875rem",
    fontWeight: "700",
    fontFamily: `"Roboto Slab", "Times New Roman", serif`,
    marginBottom: "1rem",
    marginTop: "30px",
    minHeight: "32px",
    textDecoration: "none"
  },
  description: {
    color: "#999",
    maxWidth: "750px"
  }
};

const useStyles = makeStyles(styles);

export default function ProductSection() {
  const classes = useStyles();
  return (
    <Grid container direction="column" alignItems="center" className={classes.productSection}>
        <Grid item>
          <Grid container alignItems="center" direction="column">
            <Grid item xs={12} sm={12} md={8} className={classes.section}>
              <Typography variant="h2" className={classes.title}>What is CleanCode?</Typography>
              <Typography variant="h5" className={classes.description}>
                LeetCode but better, much better! One stop shop for to ace all developing interviews,
                algorithm live coding, system design white boarding, conceptual and behavior phone interview,
                anything the company can come with, we have it! 
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12} md={4}>
              <InfoArea
                title="1000+ Algorithm Problems"
                description="We have every algorithm problem you can find online, with detailed solution on every single one of them"
                icon={EventNote}
                vertical
              />
            </Grid>
            <Grid item xs={12} sm={12} md={4}>
              <InfoArea
                title="50+ System Design Problems"
                description="We have the largest system design problem collection, detailed solution to each with exclusive industry insights on company's scoring critera"
                icon={SettingsSystemDaydreamIcon}
                vertical
              />
            </Grid>
            <Grid item xs={12} sm={12} md={4}>
              <InfoArea
                title="100+ Conceptual and Behavior Problems"
                description="Most interviewees get elimated in the first round when they can't answer conceptual details about their projects. You won't need to worry about it anymore"
                icon={CodeIcon}
                vertical
              />
            </Grid>
            <Grid item xs={12} sm={12} md={4}>
              <InfoArea
                title="Interview FastTrack"
                description="Overwhelmed by all the problems? You don't need to do all the problem or any 10 percent of the problems. We have courses designed to get you ramped up in a few weeks to a couple of month"
                icon={FastForwardIcon}
                vertical
              />
            </Grid>
            <Grid item xs={12} sm={12} md={4}>
              <InfoArea
                title="Specific Courses Tailored to Your Need"
                description="Good at algorithms but just needs a little help on system design? Or vice versa? We have specific courses tailor to what you look for"
                icon={ClassIcon}
                vertical
              />
            </Grid>
            <Grid item xs={12} sm={12} md={4}>
              <InfoArea
                title="Community and 24/7 Custom Service Support"
                description="Never feel you are alone in your journey! We are always here to help to answer any questions or concerns"
                icon={Chat}
                vertical
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
  );
}