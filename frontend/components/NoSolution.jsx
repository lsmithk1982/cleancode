import {
  Grid,
  Box,
  makeStyles,
  Typography
} from "@material-ui/core";

const styles = (theme) => ({
  container: {
    height: "100%",
    width: "100%"
  },
  text: {
    color: "#e0e0e0"
  }
});
const useStyles = makeStyles(styles);

function NoSolution() {
  const classes = useStyles();
  return (
    <Grid container alignItems="center" justify="center" className={classes.container}>
      <Grid item>
        <Typography variant="h1" className={classes.text}>
          No Solution Yet
        </Typography>
      </Grid>
    </Grid>
  )
}

export default NoSolution;