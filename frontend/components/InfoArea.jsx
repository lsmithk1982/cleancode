import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import { 
  Grid,
  Typography
} from "@material-ui/core";

const styles = {
  infoArea: {
    maxWidth: "360px",
    margin: "0 auto",
    padding: "0px"
  },
  iconWrapper: {
    // float: "left",
    marginTop: "24px",
    marginRight: "10px"
  },
  icon: {
    width: "36px",
    height: "36px"
  },
  descriptionWrapper: {
    color: "gray",
    overflow: "hidden"
  },
  title: {
    color: "#3C4858",
    margin: "1.75rem 0 0.875rem",
    textDecoration: "none",
    fontWeight: "700",
    fontFamily: `"Roboto Slab", "Times New Roman", serif`
  },
  description: {
    color: "gray",
    overflow: "hidden",
    marginTop: "0px",
    fontSize: "14px"
  },
  iconWrapperVertical: {
    float: "none"
  },
  iconVertical: {
    width: "61px",
    height: "61px"
  }
};

const useStyles = makeStyles(styles);

export default function InfoArea({ title, description, iconColor, vertical, ...rest }) {
  const classes = useStyles();
  const iconWrapper = clsx({
    [classes.iconWrapper]: true,
    [classes.iconWrapperVertical]: vertical
  });
  const iconClasses = clsx({
    [classes.icon]: true,
    [classes.iconVertical]: vertical
  });
  return (
    // <div className={classes.infoArea}>
    //   <div className={iconWrapper}>
    //     <rest.icon className={iconClasses} />
    //   </div>
    //   <div className={classes.descriptionWrapper}>
    //     <h4 className={classes.title}>{title}</h4>
    //     <p className={classes.description}>{description}</p>
    //   </div>
    // </div>
    <Grid container direction="column" alignItems="center" className={classes.infoArea}>
      <Grid item className={iconWrapper}>
        <rest.icon className={iconClasses} />
      </Grid>
      <Grid item>
        <Typography variant="h4" className={classes.title}>{title}</Typography>
      </Grid>
      <Grid item>
        <Typography variant="body1">{description}</Typography>
      </Grid>
    </Grid>
  );
}