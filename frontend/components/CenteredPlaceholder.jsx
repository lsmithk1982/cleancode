import {
  Grid,
  Box,
  makeStyles,
  Typography
} from "@material-ui/core";

const styles = (theme) => ({
  container: {
    height: "100%",
    width: "100%",
    minHeight: "6.5rem"
  },
  text: {
    color: "#e0e0e0"
  }
});
const useStyles = makeStyles(styles);

function CenteredPlaceholder({ text }) {
  const classes = useStyles();
  return (
    <Grid container alignItems="center" justify="center" className={classes.container}>
      <Grid item>
        <Typography variant="h1" className={classes.text}>
          {text}
        </Typography>
      </Grid>
    </Grid>
  )
}

export default CenteredPlaceholder;