import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  makeStyles,
  IconButton,
  Button,
  Typography,
  Box,
  Grid,
  TextField
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import Graph from "vis-react";
import Dot from "./Dot";
// import { Graphviz } from 'graphviz-react';

const styles = (theme) => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  dialogActions: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3)
  },
  textFieldRoot: {
    borderRadius: 20
  }
});

const useStyles = makeStyles(styles);

const nodeArrayToGraphObj = (nodesArray) => {
  console.log("called");
  const nodes = nodesArray.map((value, index) => ({
    id: value == null ? -1 : index,
    label: value
  })).filter(node => node.id > -1 );
  console.log("nodes", nodes);
  const newGraph = {
    nodes: [],
    edges: []
  }
  for(let i = 0; i < nodesArray.length; i++){
    console.log("i", i);
    if(nodesArray[i] == null){
      if(i*2+1 < nodesArray.length){
        nodesArray.splice(i*2+1, 0, null);
        console.log("nodesArray", nodesArray);
      }
      if(i*2+2 < nodesArray.length){
        nodesArray.splice(i*2+2, 0, null);
        console.log("nodesArray1", nodesArray);
      }
      continue;
    }
    newGraph.nodes.push({id: i, label: nodesArray[i]});
    if(i*2+1 < nodesArray.length){
      console.log("here");
      const leftChild = nodesArray[i*2+1];
      console.log("leftchild", leftChild);
      if(leftChild != null){
        console.log(i*2+1, leftChild);
        const edgeLeft= { from: i, to: i*2+1 };
        newGraph.edges = [...newGraph.edges, edgeLeft]
      }
      console.log("graph1", newGraph);
    }
    if(i*2+2 < nodesArray.length){
      const rightChild = nodesArray[i*2+2];
      if(rightChild != null){
        const edgeRight = { from: i, to: i*2+2 };;
        newGraph.edges = [...newGraph.edges, edgeRight]
      }
    }
  }

  return newGraph;
}

const linkedListToGraphObj = (linkedlistObj) => {
  // const newGraph = {
  //   nodes: [],
  //   edges: []
  // };
  // for (const [key, value] of Object.entries(linkedlistObj)) {
  //   console.log(`${key}: ${value}`);
  //   console.log("here");
  //   if(!newGraph.nodes.includes(key)) {
  //     newGraph.nodes.push({ id: key, label: `Node ${key}` });
  //   }
  //   if(!newGraph.nodes.includes(value)) {
  //     newGraph.nodes.push({ id: value, label: `Node ${value}` });
  //   }
  //   newGraph.edges.push({ from: key, to: value});
  // }
  let newGraph = {
    nodes: [
        { id: 1, label: 'Node 1' },
        { id: 2, label: 'Node 2' },
        { id: 3, label: 'Node 3'},
        { id: 4, label: 'Node 4'},
        { id: 5, label: 'Node 5'},
        { id: 6, label: 'Node 6'},
        { id: 7, label: 'Node 7'}
    ],
    edges: [
        { from: 1, to: 2 },
        { from: 2, to: 3 },
        { from: 3, to: 4},
        { from: 4, to: 5},
        { from: 5, to: 6},
        { from: 6, to: 7}
    ]
  };
  console.log("newgraph", newGraph);
  return newGraph;
}

const treeOption = {
  layout: {
      hierarchical: {
        enabled: true,
        sortMethod: "directed",
        nodeSpacing: 200
      }
  },
  edges: {
      color: '#000000'
  },
  interaction: { hoverEdges: false },
  physics: { "enabled": false, solver: 'hierarchicalRepulsion', stabilization: { fit: true } },
  // nodes: { fixed: true },
  height: "100%",
  width: "100%"
};

const linkedListOption = {
  layout: {
      hierarchical: {
        enabled: false,
        sortMethod: "directed",
        nodeSpacing: 200,
        direction: "LR"
      }
  },
  edges: {
      color: '#000000'
  },
  interaction: { hoverEdges: false },
  physics: { "enabled": true, solver: 'hierarchicalRepulsion', stabilization: { fit: true } },
  // nodes: { fixed: true },
  height: "100%",
  width: "100%"
};

const graphOption = {
  layout: {
      hierarchical: {
        enabled: true,
        sortMethod: "directed",
        nodeSpacing: 200
      }
  },
  edges: {
      color: '#000000'
  },
  interaction: { hoverEdges: false },
  physics: { "enabled": false, solver: 'hierarchicalRepulsion', stabilization: { fit: true } },
  // nodes: { fixed: true },
  height: "100%",
  width: "100%"
};

const dataType2options = {
  "tree": treeOption,
  "linkedList": linkedListOption,
  "graph": graphOption
}

function EditInputModal({ open=false, handleClose, handleConfirm, args=[{type: "linkedList", data: {"1":"2"}}, {type: "tree", data: [1,2,3,4,5]}] }) {
  const classes = useStyles();
  const options = {
    layout: {
        hierarchical: {
          enabled: true,
          sortMethod: "directed",
          nodeSpacing: 200
        }
    },
    edges: {
        color: '#000000'
    },
    interaction: { hoverEdges: false },
    physics: { "enabled": false, solver: 'hierarchicalRepulsion', stabilization: { fit: true } },
    // nodes: { fixed: true },
    height: "100%",
    width: "100%"
  };
  const events = {
    select: function(event) {
      var { nodes, edges } = event;
      currentNetwork.fit({ nodes: nodes });
    },
    animationFinished: (event) => {
      console.log("finished");
      currentNetwork.fit();
    }
  };
  // let newGraph = {
  //   nodes: [
  //       { id: 1, label: 'Node 1' },
  //       { id: 2, label: 'Node 2' },
  //       { id: 3, label: 'Node 3'},
  //       { id: 4, label: 'Node 4'},
  //       { id: 5, label: 'Node 5'},
  //       { id: 6, label: 'Node 6'},
  //       { id: 7, label: 'Node 7'}
  //   ],
  //   edges: [
  //       { from: 1, to: 2 },
  //       { from: 1, to: 3 },
  //       { from: 2, to: 4},
  //       { from: 2, to: 5},
  //       { from: 5, to: 6},
  //       { from: 6, to: 7}
  //   ]
  // };
  // const [graph, setGraph] = React.useState(newGraph);
  // const [network, setNetwork] = React.useState(null);
  const [currentGraphData, setCurrentGraphData] = React.useState({
    nodes: [],
    edges: []
  });
  const [currentOptions, setCurrentOptions] = React.useState(null);
  const [currentNetwork, setCurrentNetwork] = React.useState(null);
  const [argus, setArgus] = React.useState(args.map((arg) => ({
    type: arg.type,
    value: JSON.stringify(arg.data)
  })));
  const [currentVisualizer, setCurrentVisualizer] = React.useState(null);

  const onTextFieldChange = (index, event) => {
    if(argus[index].type == "tree"){
      try {
        const nodesArray = JSON.parse(event.target.value);
        const nextGraph = nodeArrayToGraphObj(nodesArray);
        setCurrentGraphData(nextGraph);
        const nextState = [...argus];
        nextState[index] = { ...nextState[index], value: event.target.value }
        setArgus(nextState);
      } catch {
        const nextState = [...argus];
        nextState[index] = { ...nextState[index], value: event.target.value }
        setArgus(nextState);
      }
    }
    else if(argus[index].type == "linkedList") {
      try {
        const linkedListObj = JSON.parse(event.target.value);
        const nextGraph = linkedListToGraphObj(linkedListObj);
        setCurrentGraphData(nextGraph);
        const nextState = [...argus];
        nextState[index] = { ...nextState[index], value: event.target.value }
        setArgus(nextState);
      } catch {
        const nextState = [...argus];
        nextState[index] = { ...nextState[index], value: event.target.value }
        setArgus(nextState);
      }
    }
  };

  const onTextFieldFocus = (index, event) => {
    if(argus[index].type == "tree"){
      try {
        const nodesArray = JSON.parse(event.target.value);
        const nextGraph = nodeArrayToGraphObj(nodesArray);
        setCurrentVisualizer("tree");
        setCurrentGraphData(nextGraph);
        setCurrentOptions(dataType2options["tree"]);
      } catch(e) {
        console.log(e);
        setCurrentVisualizer("tree");
        setCurrentGraphData({
          nodes: [],
          edges: []
        });
        setCurrentOptions(dataType2options["tree"]);
      }
    } 
    else if(argus[index].type == "linkedList"){
      try {
        const linkedListObj = JSON.parse(event.target.value);
        console.log("link", linkedListObj);
        const nextGraph = linkedListToGraphObj(linkedListObj);
        setCurrentVisualizer("linkedList");
        setCurrentGraphData(nextGraph);
        setCurrentOptions(dataType2options["linkedList"]);
      } catch(e) {
        console.log(e);
        setCurrentVisualizer("linkedList");
        setCurrentGraphData({
          nodes: [],
          edges: []
        });
        setCurrentOptions(dataType2options["linkedList"]);
      }
    }
  }

  React.useEffect(() => {
    console.log("okay");
    if(currentNetwork){
      currentNetwork.redraw();
      currentNetwork.fit({
        animation: false
      });
    }
  }, [currentGraphData, currentNetwork])

  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="edit-problem-dialog-title"
      aria-describedby="edit-problem-dialog-description"
      maxWidth="xs"
      fullWidth
    >
      <DialogTitle disableTypography>
        <Typography variant="h4">
          {"Custom Test Case"}
        </Typography>
        <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent dividers>
          {(() => {
            if(currentVisualizer == "tree" || currentVisualizer == "linkedList"){
              return (
                <Box height={300}>
                  {/* <Graph
                    graph={currentGraphData}
                    options={currentOptions}
                    events={events}
                    getNetwork={(data) => { setCurrentNetwork(data) }}
                  /> */}
                  <Dot />
                </Box>
              )
            }
          })()}
      </DialogContent>
      <DialogContent dividers>
        <Grid container direction="column" spacing={1}>
          {argus.map((argu, index) => {
            return (
              <Grid item key={index}>
                <Grid container alignItems="center" spacing={1}>
                  <Grid item>
                    {`Argument ${index+1} (${argu.type}): `}
                  </Grid>
                  <Grid item style={{ flex: 1, minWidth: 0 }}>
                    <TextField 
                      fullWidth
                      value={argu.value}
                      onChange={onTextFieldChange.bind({}, index)}
                      onFocus={onTextFieldFocus.bind({}, index)}
                      autoFocus={index == 0}
                      variant="outlined"
                      classes={{root: classes.textFieldRoot}}
                    />
                  </Grid>
                </Grid>
              </Grid>
            )
          })}
        </Grid>
      </DialogContent>
      <DialogActions className={classes.dialogActions}>
        <Button 
          variant="outlined" 
          onClick={() => { currentNetwork.fit() }}
          color="primary"
        >
          {"Cancel"}
        </Button>
        <Button 
            variant="contained" 
            onClick={() => { setGraph({
              nodes: [...graph.nodes, { id: graph.nodes[graph.nodes.length-1].id + 1, label: `Node ${graph.nodes[graph.nodes.length-1].id + 1}`}],
              edges: [...graph.edges, { from: graph.edges[graph.edges.length-1].to, to: graph.edges[graph.edges.length-1].to + 1}]
            }) }}  
            color="primary"
          >
          {"Confirm"}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default EditInputModal;