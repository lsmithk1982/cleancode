import {
    Grid,
    Box,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper,
    makeStyles,
    Collapse,
    IconButton,
    Typography
} from "@material-ui/core";
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline';
import uuid from 'react-uuid';

const styles = (theme) => ({
    root: {
        '& > *': {
          borderBottom: 'unset',
        },
    },
    table: {
        minWidth: 650,
    },
});

const useStyles = makeStyles(styles);

// function createData(name, calories, fat, carbs, protein) {
//     return { name, calories, fat, carbs, protein };
// }

// const rows = [
//     createData('Frozen yoghurt', 159, 6.0, 24, 4.0)
// ];
const sampleOuterData = {
  "Time": {
    "toolTip": null,
    "value": "11/14/2020 01:47"
  },
  "Language": {
    "toolTip": null,
    "value": "Javascript"
  },
  "Test Passed": {
    "toolTip": null,
    "value": "1/1"
  },
  "Evaluation": {
    "toolTip": null,
    "value": "Optimal"
  }
};

const sampleInnerData = {
  "Run Time": {
    "toolTip": null,
    "value": "15 ms"
  },
  "Optimal Run Time": {
    "toolTip": null,
    "value": "13 ms"
  },
  "Memory Usage": {
    "toolTip": null,
    "value": "50.3 mb"
  },
  "Optimal Memory Usage": {
    "toolTip": null,
    "value": "1.5 mb"
  },
}

function CollapsibleTable({ outerData=sampleOuterData, innerData=sampleInnerData }) {
    const classes = useStyles();
    const [open, setOpen] = React.useState(true);
   
    return (
        <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell />
                        {Object.keys(outerData).map((key) => (
                            <TableCell key={uuid()}>
                              <Grid container>
                                <Grid item>
                                  {key}
                                </Grid>
                                <Grid item>
                                  <HelpOutlineIcon fontSize="small"/>
                                </Grid>
                              </Grid>
                            </TableCell>
                        ))}
                    </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow className={classes.root}>
                      <TableCell>
                        <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                        </IconButton>           
                      </TableCell>
                      {Object.values(outerData).map((value) => (
                        <TableCell key={uuid()}>
                          {value.value}
                        </TableCell>
                      ))}
                    </TableRow>
                    <TableRow>
                      <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                        <Collapse in={open} timeout="auto" unmountOnExit>
                          <Box margin={1}>
                            <Typography variant="h6" gutterBottom component="div">
                              Details
                            </Typography>
                            <Table size="small" aria-label="data-details">
                              <TableHead>
                                <TableRow>
                                    {Object.keys(innerData).map((key) => (
                                        <TableCell key={uuid()}>
                                            {key}
                                        </TableCell>
                                    ))}
                                </TableRow>
                              </TableHead>
                              <TableBody>
                                <TableRow>
                                  {Object.values(innerData).map((value) => (
                                    <TableCell key={uuid()}>
                                      {value.value}
                                    </TableCell>
                                  ))}
                                </TableRow>
                              </TableBody>
                            </Table>
                          </Box>
                        </Collapse>
                      </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default CollapsibleTable;