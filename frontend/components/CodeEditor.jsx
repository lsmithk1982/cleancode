import dynamic from 'next/dynamic';
const CodeMirror = dynamic(() => {
  import('codemirror/mode/javascript/javascript');
  import('codemirror/mode/python/python');
  import('codemirror/mode/clike/clike');
  import('codemirror/addon/comment/comment');
  import('codemirror/addon/selection/active-line.js');
  import('codemirror/mode/shell/shell');
  import('codemirror/keymap/emacs.js');
  import('codemirror/keymap/sublime.js');
  import('codemirror/keymap/vim.js');
  return import("react-codemirror2").then((mod) => mod.Controlled);
}, { ssr: false });
import {
  Grid,
  Box
} from "@material-ui/core"
// let modeLoaded = false;
// if (typeof window !== 'undefined' && typeof window.navigator !== 'undefined') {
//   require('codemirror/mode/javascript/javascript');
//   require('codemirror/mode/python/python');
//   require('codemirror/mode/clike/clike');
//   modeLoaded = true;
// };
const toggleComment = editor => {
  console.log("editorToggle", editor);
  editor.toggleComment({indent: true});
}
const saveCursorPosition = editor => {
  console.log("editor", editor);  
  const cursorObj = editor.doc.getCursor();
  console.log("line", cursorObj.line);
  console.log("ch", cursorObj.ch);
}

function CodeEditor({
  configOptions, 
  mode="javascript", 
  height="100%", 
  width="100%",
  setCodeInputs=null,
  codeInputs,
  fontSize="16px",
  readOnly=false
}){
  const codeMirrorOptions = {
    theme: "default",
    lineNumbers: true,
    scrollbarStyle: "native",
    lineWrapping: false,
    readOnly: readOnly,
    styleActiveLine: true,
    extraKeys: { 'Cmd-/': toggleComment, 'Ctrl-/': toggleComment},
    ...configOptions
  };

  return CodeMirror && ( 
    <Box height={height} width={width} fontSize={fontSize}>
      <CodeMirror
        options={{
          ...codeMirrorOptions,
          mode: mode
        }}
        value={codeInputs ? codeInputs[mode] : ""}
        onBeforeChange={!readOnly ? (editor, data, value) => {
          setCodeInputs({
            ...codeInputs,
            [mode]: value
          }); 
        } : null}
      /> 
    </Box>
  )
}

export default CodeEditor;