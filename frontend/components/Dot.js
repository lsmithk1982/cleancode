import { graphviz } from "d3-graphviz";
import React, { Component } from "react";

const defaultDot = `digraph{ 
    rankdir = "LR"
    1 -> 2;
    2 -> 3;
    3 -> 1;
    3 -> 4;
    4 -> 3;
  }`;

function Dot({ dot=defaultDot }) {
    console.log("dot", dot);
    const draw = () => {
        graphviz(`#graph-body`)
          .options({
            height: "100%",
            width: "100%",
            fit: true
          })
          .renderDot(dot);
    }

    React.useEffect(() => {
        draw();
    }, [dot])

    return(
        <div id="graph-body" style={{height: "100%", width: "100%"}}></div>
    ) 
}

export default Dot;
// class Dot extends Component{

//     constructor(props) {
//         super();
//         this.draw = this.draw.bind(this);
//     }

//     componentDidMount() {
//         this.draw()
//     }

//     draw(){
//         const { dot } = this.props;
//         graphviz(`#graph-body`)
//           .options({
//             height: "100%",
//             width: "100%",
//             fit: true
//           })
//           .renderDot(`digraph{ 
//             rankdir = "LR"
//             1 -> 2;
//             2 -> 3;
//             3 -> 1;
//             3 -> 4;
//             4 -> 3;
//           }`);
//     }

//     render(){
//         return(
//             <div id="graph-body" style={{height: "100%", width: "100%"}}></div>
//         )
//     }
// }
// export default Dot;