import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  makeStyles,
  Grid,
  Box,
  Button
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import DropDown from "./DropDown";

const fontSizeOptions = [{value: "12px", text: "12px"}, {value: "13px", text: "13px"},
                         {value: "14px", text: "14px"}, {value: "15px", text: "15px"},
                         {value: "16px", text: "16px"}, {value: "17px", text: "17px"},
                         {value: "18px", text: "18px"}, {value: "19px", text: "19px"},
                         {value: "20px", text: "20px"}, {value: "21px", text: "21px"}];
const tabSpaceingOptions = [{value: 2, text: "2 spaces"}, {value: 4, text: "4 spaces"}, {value: 8, text: "8 spaces"}];
const themeOptions = [{value: "default", text: "default"}, {value: "dracula", text: "dracula"},
                      {value: "eclipse", text: "eclipse"}, {value: "material", text: "material"},
                      {value: "rubyblue", text: "rubyblue"}, {value: "zenburn", text: "zenburn"}];
const keyBindingOptions = [{value: "default", text: "default"}, {value: "emacs", text: "emacs"},
                           {value: "sublime", text: "sublime"}, {value: "vim", text: "vim"}]

const styles = (theme) => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  dialogActions: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3)
  }
});

const useStyles = makeStyles(styles);

function EditorSettingModal({ open, handleClose, settings, handleChange, handleApply, handleReset}) {
  const classes = useStyles();

  return (
    <Dialog maxWidth="xs" open={open} onClose={handleClose} aria-labelledby="setting-dialog" fullWidth>
      <DialogTitle>
        Setting
        <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent dividers>
        <Grid container direction="column" spacing={2}>
          <Grid item style={{width: "100%"}}>
            <Grid container alignItems="center" justify="space-between">
              <Grid item>
                Font Size
              </Grid>
              <Grid item>
                <DropDown 
                  label={"font size"} 
                  items={fontSizeOptions} 
                  value={settings.fontSize} 
                  onChange={handleChange.bind({}, "fontSizeDropdown")} 
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item style={{width: "100%"}}>
            <Grid container alignItems="center" justify="space-between">
              <Grid item>
                Tab Spaces
              </Grid>
              <Grid item>
                <DropDown 
                  label={"Tab Spaces"} 
                  items={tabSpaceingOptions} 
                  value={settings.tabSize} 
                  onChange={handleChange.bind({}, "tabSizeDropdown")} 
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item style={{width: "100%"}}>
            <Grid container alignItems="center" justify="space-between">
              <Grid item>
                Theme
              </Grid>
              <Grid item>
                <DropDown 
                  label={"theme"} 
                  items={themeOptions} 
                  value={settings.theme} 
                  onChange={handleChange.bind({}, "themeDropdown")} 
                />
              </Grid>
            </Grid>
          </Grid>
          <Grid item style={{width: "100%"}}>
            <Grid container alignItems="center" justify="space-between">
              <Grid item>
                Short Cut Key Bindings
              </Grid>
              <Grid item>
                <DropDown 
                  label={"key bindings"} 
                  items={keyBindingOptions} 
                  value={settings.keyBinding} 
                  onChange={handleChange.bind({}, "keyBindingDropdown")} 
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions className={classes.dialogActions}>
        <Button 
            variant="outlined" 
            onClick={handleReset}  
            color="primary"
          >
          Reset to default
        </Button>
        <Button 
            variant="contained" 
            onClick={handleApply}  
            color="primary"
          >
          Apply
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default EditorSettingModal;