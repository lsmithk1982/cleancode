import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputAdornment from '@material-ui/core/InputAdornment';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import EmailIcon from '@material-ui/icons/Email';
import LockIcon from '@material-ui/icons/Lock';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import { Alert, AlertTitle } from '@material-ui/lab';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import { 
  signUpWithEmailPassword,
  sendVerificationEmail
} from '../firebase/auth';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  signUpButton: {
    "margin-bottom": theme.spacing(3)
  },
  signInButton: {
    "margin-bottom": theme.spacing(3)
  }
}));

function SignUpModal({ setOpen, open, className=0, setLoginOpen, ...rest }) {
  const classes = useStyles();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('sm'));
  const [values, setValues] = React.useState({
    email: '',
    userName: '',
    password: '',
    showPassword: false,
  });
  const [error, setError] = React.useState("");

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  };

  const handleSignUp = async () => {
    let createdUser;
    try {
      createdUser = await signUpWithEmailPassword(values.email, values.password, values.userName);
    }
    catch(err){
      setError(err.message);
    }
    createdUser && await sendVerificationEmail();
  };

  const handlePressEnterKey = async (event) => {
    if(event.key == "Enter"){
      await handleSignUp();
    }
  }

  return (
    <>
      <Button variant="contained" className={clsx(classes.registerbutton, className)} color="primary" onClick={handleClickOpen} {...rest}>
        Sign Up
      </Button>
      <Dialog maxWidth="xs" open={open} onClose={handleClose} aria-labelledby="Sign Up-dialog">
        <DialogTitle id="form-dialog-title">
           Sign Up and Start Learning!
          <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
            <CloseIcon />
          </IconButton>
        </DialogTitle>
        <DialogContent dividers>
          {error &&
          <Alert severity="error">
            {/* <AlertTitle>Error</AlertTitle> */}
            {error}
          </Alert>
          }
          <TextField
            autoFocus
            margin="normal"
            id="emailText"
            label="Email Address"
            type="email"
            value={values.email}
            onChange={handleChange('email')}
            variant="outlined"
            fullWidth
            placeholder="Email"
            onKeyPress={handlePressEnterKey}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <EmailIcon />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            id="userNameText"
            label="User Name"
            value={values.userName}
            onChange={handleChange('userName')}
            fullWidth
            variant="outlined"
            margin="normal"
            placeholder="User Name"
            onKeyPress={handlePressEnterKey}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <AccountCircleIcon />
                </InputAdornment>
              ),
            }}
          />
          <TextField
            id="password"
            type={values.showPassword ? 'text' : 'password'}
            value={values.password}
            onChange={handleChange('password')}
            label="Password"
            variant="outlined"
            margin="normal"
            fullWidth
            placeholder="Password"
            onKeyPress={handlePressEnterKey}
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">
                  <LockIcon />
                </InputAdornment>
              ),
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                  >
                    {values.showPassword ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              )
            }}
          />
        </DialogContent>
        <DialogActions className={classes.signUpButton}>
          <Button fullWidth variant="contained" onClick={handleSignUp} fullWidth color="primary">
            Sign Up
          </Button>
        </DialogActions>
        <Box alignSelf="center">
          <span style={{verticalAlign: "top"}}>
            {"Already have an account? "}
          </span>
          <Link
              className={classes.signInButton}
              component="button"
              variant="body2"
              onClick={()=>{setOpen(false); setLoginOpen(true)}}
            >
             Log in
          </Link>
        </Box>
      </Dialog>
    </>
  )
}

export default SignUpModal;