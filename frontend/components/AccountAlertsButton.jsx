import NotificationsIcon from '@material-ui/icons/Notifications';
import { IconButton, Badge } from '@material-ui/core';

function AccountAlertsButton({ alertsNum=3 }) {
  return (
    <IconButton
      edge="start"
      color="inherit"
      aria-label="open account menu"
      onClick={() => console.log("account")}
    >
      <Badge badgeContent={alertsNum} color="primary">
        <NotificationsIcon fontSize="large" />
      </Badge>
    </IconButton>
  );
}

export default AccountAlertsButton;