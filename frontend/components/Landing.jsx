import Parallax from "./Parallax";
import Header from "./Header";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import ProductSection from "./ProductSection";
import Hero from "./Hero";
import SecondHero from "./SecondHero";
import ThirdHero from "./ThirdHero";
import TeamSection from "./TeamSection";
import Footer from "./Footer";

const styles = {
  container: {
    zIndex: "12",
    color: "#FFFFFF",
    // paddingRight: "15px",
    // paddingLeft: "15px",
    // margin: "auto",
    // marginLeft: "auto",
    // margin
    // width: "100%",
    // "@media (min-width: 576px)": {
    //   maxWidth: "540px"
    // },
    // "@media (min-width: 768px)": {
    //   maxWidth: "720px"
    // },
    // "@media (min-width: 992px)": {
    //   maxWidth: "960px"
    // },
    // "@media (min-width: 1200px)": {
    //   maxWidth: "1140px"
    // }
  },
  title: {
    // fontWeight: "700",
    // fontFamily: `"Roboto Slab", "Times New Roman", serif`,
    // color: "#FFFFFF",
    // textDecoration: "none"
  },
  subtitle: {
    // fontSize: "1.313rem",
    // maxWidth: "500px",
    // margin: "10px auto 0"
  },
  button: {
    marginLeft: "auto",
    marginRight: "auto"
  },
  main: {
    background: "#FFFFFF",
    position: "relative",
    zIndex: "3"
  },
  mainRaised: {
    margin: "-60px 30px 0px",
    borderRadius: "6px",
    boxShadow:
      "0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)"
  }
};

const useStyles = makeStyles(styles);

function Landing() {
  const classes = useStyles();
  return (
    <>
      <Header />
      <Parallax filter responsive image={"/img/landing-bg.jpg"}>
        <div className={classes.container}>
          <Grid container justify="center">
            <Grid item xs={12} sm={12} md={6}>
              <Grid container direction="row" justify="center">
                <Grid item>
                  <h1 className={classes.title}>Your Career Starts With Us.</h1>
                </Grid>
                <Grid item>
                  <h3>
                    Tired of leetcode? Hard to understand solutions?
                    Doesn't feel like you made any progress despite doing hundreds
                    of them? Wants to learn system design and patterns?
                    Wants to be prepared for all aspects of interview? 
                    The answer to all your worries is just one click away.
                  </h3>
                </Grid>
                <Grid item>
                  <Button
                    className={classes.button}
                    color="primary"
                    variant="contained"
                    size="large"
                    href="https://www.youtube.com/watch?v=dQw4w9WgXcQ&ref=creativetim"
                    target="_blank"
                    rel="noopener noreferrer"
                  >
                    Watch video
                  </Button>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </div>
      </Parallax>
      <ProductSection />
      <Hero />
      <SecondHero />
      <ThirdHero />
      <TeamSection />
      <Footer />
    </>
  );
}

export default Landing;