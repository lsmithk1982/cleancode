import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { 
  IconButton,
  Menu,
  ListItemText,
  ListItemIcon,
  MenuItem
 } from '@material-ui/core';
 import ExitToAppIcon from '@material-ui/icons/ExitToApp';
 import { auth } from '../firebase/auth';
 import { UserContext } from "./providers/UserProvider";
 import Router from 'next/router';

function ProfileButton() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const { setLoading } = React.useContext(UserContext);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const signOut = () => {
    // setLoading(true);
    auth.signOut().then(() => {
      Router.push("/");
    })
  }
  
  return (
    <>
      <IconButton
        edge="start"
        color="inherit"
        aria-label="open account menu"
        onClick={handleClick}
      >
        <AccountCircleIcon fontSize="large" />
      </IconButton>
      <Menu
        elevation={0}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={signOut}>
          <ListItemIcon>
            <ExitToAppIcon />
          </ListItemIcon>
          <ListItemText primary="Log out" />
        </MenuItem>
      </Menu>
  </>
  );
}

export default ProfileButton;
