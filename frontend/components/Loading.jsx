import {
  Grid,
  CircularProgress,
  makeStyles
} from "@material-ui/core"
import clsx from "clsx";


const styles = (theme) => ({
  container: {
    height: "100%",
    width: "100%",
    minHeight: "8rem"
  }
});
const useStyles = makeStyles(styles);

function Loading({ open, ...className }) {
  const classes = useStyles();
  return (
    <Grid container alignItems="center" justify="center" className={clsx(classes.container, className)}>
      <Grid item>
        <CircularProgress />
      </Grid>
    </Grid>
  )
}

export default Loading;