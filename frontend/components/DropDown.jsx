import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  makeStyles
} from "@material-ui/core";
import uuid from "react-uuid";
import clsx from "clsx";

const styles = {
  formControl: {
    minWidth: 150,
    maxHeight: 60
  },
  inputLabel: {
    color: "black"
  },
};
const useStyles = makeStyles(styles);

function DropDown({ items, label, value, onChange, className="", ...rest}) {
  const classes = useStyles();
  return (
    <FormControl variant="outlined" color="secondary" className={clsx(classes.formControl, className)} {...rest}>
      <InputLabel id="code-language-selection-label" className={classes.inputLabel}>{label}</InputLabel>
      <Select
        labelId="fontsize-selection-label"
        id="fontsize-select-outlined"
        value={value}
        onChange={onChange}
        label="fontSize"
        name="golovkin"
        style={{textAlign: "start"}}
      >
        {items.map((item) => {
          return <MenuItem key={uuid()} value={item.value}>{item.text}</MenuItem>
        })}
      </Select>
    </FormControl>
  )
}

export default DropDown;