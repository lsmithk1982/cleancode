import {
  Grid,
  Box,
  Typography,
  makeStyles,
  Divider,
  InputLabel,
  MenuItem,
  FormHelperText,
  FormControl,
  Select,
} from "@material-ui/core";
import CodeEditor from "./CodeEditor";
import uuid from 'react-uuid';

const styles = (theme) => ({
  langaugeDropDown: {
    marginRight: "auto"
  },
  formControl: {
    minWidth: 120,
    maxHeight: "50px"
  },
  inputLabel: {
    color: "black"
  },
});

const useStyles = makeStyles(styles);

function CodeEditorReadOnly({ solution }) {
  const classes = useStyles();
  const [language, setLanguage] = React.useState("text/x-java");
  const handleChange = (event) => {
    setLanguage(event.target.value);
  };

  return (
    <Grid container direction="column" spacing={2}>
      <Grid item className={classes.langaugeDropDown}>
        <FormControl variant="outlined" color="secondary" className={classes.formControl}>
          <InputLabel id="code-language-selection-label" className={classes.inputLabel}>Language</InputLabel>
          <Select
            labelId="language-selection-label"
            id="language-select-outlined"
            value={language}
            onChange={handleChange}
            label="Javascript"
          >
            {Object.keys(solution.codeSol).map(language => {
              let label = language.toUpperCase();
              if(language == "text/x-java"){
                label = "JAVA"
              }
              return <MenuItem key={uuid()} value={language}>{label}</MenuItem>
            })}
          </Select>
        </FormControl>
      </Grid>
      <Grid item>
        <CodeEditor readOnly={true} mode={language} codeInputs={solution.codeSol} />
      </Grid>
    </Grid>
  )
};

function renderSolution(solution, i) {
  // localStorage.setItem(`${problem.id}_lastLan`, language);
  return (
    <Grid container direction="column" key={uuid()} spacing={2}>
      <Grid item>
        <Typography variant="h4">
          {`Solution ${Number(i) + 1}: ${solution.title}`}
        </Typography>
      </Grid>
      <Grid item>
        <Typography>
          <strong>Algorithm</strong>
        </Typography>
      </Grid>
      <Grid item>
        {solution.paragraphs.map((paragraph) => {
          return <Typography key={uuid()} >{paragraph}</Typography>
        })}
      </Grid>
      <Grid item>
        <Typography>
          <strong>Code</strong>
        </Typography>
      </Grid>
      <Grid item>
        <CodeEditorReadOnly solution={solution}/>
      </Grid>
      <Grid item>
        <Typography>
          <strong>Complexity</strong>
        </Typography>
      </Grid>
      <Grid item>
        <Typography>
          {`Time Complexity: ${solution.timeComplexity}`}
        </Typography>
      </Grid>
      <Grid item>
        <Typography>
          {`Space Complexity: ${solution.spaceComplexity}`}
        </Typography>
      </Grid>
      <Grid item>
        <Divider variant="fullWidth"/>
      </Grid>
    </Grid>
  )
}

function Solution({ problem }) {
  return (
    <Box>
      <Grid container direction="column" spacing={2}>
        <Grid item>
          <Typography variant="h2">
            Solution
          </Typography>
        </Grid>
        <Grid item>
          <Divider variant="fullWidth"/>
        </Grid>
        <Grid item style={{minHeight: 0}}>
          {problem.solutions.map((solution, i) => {
            return renderSolution(solution, i)
          })}
        </Grid>
      </Grid>
    </Box>
  )
}

export default Solution;