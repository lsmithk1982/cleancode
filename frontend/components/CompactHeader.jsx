import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import SignUpModal from "./SignUpModal";
import LoginModal from "./LoginModal";
import ProfileButton from './ProfileButton';
import AccountAlertsButton from './AccountAlertsButton';
import { UserContext } from "../components/providers/UserProvider";
import ForgotPasswordModal from "./ForgotPasswordModal";

const useStyles = makeStyles((theme) => ({
    logo: {
      height: "2.5em",
      width:  "2.5em"
    },
    search: {
      height: "2.5em"
    },
    navBar: {
      height: "100%",
      backgroundColor: "white",
      paddingLeft: "5%",
      paddingRight: "5%"
    },
    userActions: {
      marginLeft: "auto"
    },
    signupButton: {
      marginLeft: theme.spacing(1)
    },
    tab: {
      minWidth: 10,
      '&:hover': {
        color: "black",
        opacity: 1
      },
      '&:focus': {
        color: "black"
      }
    },
    hamburger: {
      width: "1.5em",
      height: "1.5em"
    },
    loginButtonMobile: {
      border: 0,
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1),
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2)
    },
    signupButtonMobile: {
      border: 0,
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1),
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2)
    }
  }));

function CompactHeader({ position="relative" }) {
    const classes = useStyles();
    const { user } = React.useContext(UserContext);
    const [signUpOpen, setSignUpOpen] = React.useState(false);
    const [loginOpen, setLoginOpen] = React.useState(false);
    const [openForgotPassword, setForgotPassword] = React.useState(false);
    const iOS = process.browser && /iPad|iPhone|iPod/.test(navigator.userAgent);

    console.log("userCompact", user);
    const handleForgotPasswordClose = () => {
        setForgotPassword(false);
    }

    const handleOpenForgotPassword = () => {
        setForgotPassword(true);
        setLoginOpen(false);
    }

    return (
        <>
            <Grid container direction="row" className={classes.navBar} justify={"flex-start"} alignItems={"center"}>
                <Grid item>
                    <img alt="company logo" src="/headphones.svg" className={classes.logo}/>
                </Grid>
                <Grid item className={classes.userActions}>
                    {user ? 
                        <Grid container direction="row" spacing={1}>
                            <Grid item>
                                <AccountAlertsButton alertsNum={user.alerts}/>
                            </Grid>
                            <Grid item>
                                <ProfileButton />
                            </Grid>
                        </Grid> :
                        <Grid container direction="row" spacing={2}>
                            <Grid item>
                                <LoginModal 
                                    setOpen={setLoginOpen} 
                                    open={loginOpen} 
                                    setSignUpOpen={setSignUpOpen}
                                    handleOpenForgotPassword={handleOpenForgotPassword}
                                />
                            </Grid>
                            <Grid item>
                                <SignUpModal 
                                    setOpen={setSignUpOpen} 
                                    open={signUpOpen} 
                                    setLoginOpen={setLoginOpen}
                                />
                            </Grid>
                        </Grid>
                    }
                </Grid>
            </Grid>
            <ForgotPasswordModal open={openForgotPassword} handleClose={handleForgotPasswordClose}/>
        </>
    )
}

export default CompactHeader;