import { 
  Backdrop,
  CircularProgress,
  Snackbar,
  Typography,
  Link
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import { useDocument } from 'react-firebase-hooks/firestore';
import { auth, sendVerificationEmail } from "../../firebase/auth";
import { getUserFromFirestore, createUsertoFireStore, db } from "../../firebase/firestore";

export const UserContext = React.createContext({ user: null });

function UserProvider({ children }) {
  const [user, setUser] = React.useState(null);
  const [loading, setLoading] = React.useState(true);
  const [emailVerifiedWarningOpen, setWarning] = React.useState(user && !user.emailVerified);
  const [verificationEmailResent, setVerificationEmailResent] = React.useState(false);

  React.useEffect(() => {
    let unsubscribeFromSnapShot;
    const unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth => {
      console.log("userAuth", userAuth);
      setLoading(true);
      if(userAuth) {
        const userRef = await createUsertoFireStore(userAuth);
        unsubscribeFromSnapShot = userRef.onSnapshot(snapShot => {
          const additionalUserData = snapShot.data();
          console.log("additionalUserData", additionalUserData);
          console.log('userAuth inside closure', userAuth);
          setUser({...userAuth, ...additionalUserData});
          setWarning(!userAuth.emailVerified);
          setLoading(false);
        });
        // const additionalUserData = await getUserFromFirestore(userAuth);
        // console.log("additionalUserData", additionalUserData);
        // setUser({ ...userAuth, ...additionalUserData});
      }
      else {
        setUser(userAuth);
        console.log("wtf");
        if(unsubscribeFromSnapShot){
          console.log("here");
          unsubscribeFromSnapShot();
          unsubscribeFromSnapShot = null;
        }
        setLoading(false);
      }
    });
    return () => { unsubscribeFromSnapShot && unsubscribeFromSnapShot(); unsubscribeFromAuth();};
  }, []);
  
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setWarning(false);
  };

  const reSendVerificationEmail = async () => {
    try {
      await sendVerificationEmail();
      setVerificationEmailResent(true);
    } catch(error) {
      setVerificationEmailResent(error.message);
    }
  }

  if(loading) {
    return (
      <Backdrop open={true} >
        <CircularProgress />
      </Backdrop>
    )
  }

  return (
    <UserContext.Provider 
      value={{
        user: user,
        setLoading: setLoading
      }}
    > 
      <Snackbar 
        open={emailVerifiedWarningOpen} 
        onClose={handleClose}
        anchorOrigin={{horizontal: 'center', vertical: 'top'}}
      >
        <Alert severity="warning" onClose={handleClose}>
          <Typography>
            Thank you for registering, 
            please go to you email and click on the verification link,
            then refresh this page.
          </Typography>
          {(() => {
            if (verificationEmailResent === false) {
              return (
                emailVerifiedWarningOpen && 
                <Link component="button" onClick={reSendVerificationEmail} >
                  <Typography>
                    Click to resend verification email to {user.email}
                  </Typography>
                </Link>
              )
            } else if (verificationEmailResent === true) {
              return (
                <Typography>
                  Email Resent Success
                </Typography>
              )
            } else {
              return (
                <Typography>
                  {verificationEmailResent}
                </Typography>
              )
            }
          })()} 
        </Alert>
      </Snackbar>
      { children }
    </UserContext.Provider>
  )
}

export default UserProvider;