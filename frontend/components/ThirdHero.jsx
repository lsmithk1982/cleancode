import {
  Box,
  Container,
  Grid,
  Typography,
  makeStyles,
} from '@material-ui/core';

const styles = (theme) => ({
  image: {
    width: "100%",
    height: "100%"
  },
  heroContainer: {
    marginTop: "100px"
  }
});

const useStyles = makeStyles(styles);

function ThirdHero({ className, ...rest }) {
  const classes = useStyles();
  return (
    <Box mt={5}>
      <Container maxWidth="lg">
        <Grid container spacing={8}>
          <Grid item md={6}>
            <Grid container direction="column" spacing={1}>
              <Grid item>
                <Typography variant="overline">
                  Third Section
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="h1">
                  Ace Algorithm Interviews
                </Typography>
              </Grid>
              <Grid item>
                <Typography variant="body1">
                  System Design is often the weakest area
                  for candidates, because there is little 
                  amount of information and resources online.
                  In this section you will learn exactly what 
                  interviewers are looking for and learn about 
                  the scoring critera many company implements.
                  The good news is that it doesn't take long to 
                  master system design, 1 to 2 weeks of studying
                  is all it takes!
                </Typography>
              </Grid>
              <Grid item>
                <Grid container spacing={3}>
                  <Grid item>
                    <Box maxWidth="10em">
                      <Typography variant="h1">
                        20+
                      </Typography>
                      <Typography variant="overline">
                        System Design Video Tutorials
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid item>
                    <Box maxWidth="10em">
                      <Typography variant="h1">
                        10+
                      </Typography>
                      <Typography variant="overline">
                        Hand Selected Problems
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid item>
                    <Box maxWidth="10em">
                      <Typography variant="h1">
                        50+
                      </Typography>
                      <Typography variant="overline">
                        More Practice Problems
                      </Typography>
                    </Box>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
          <Grid item md={6}>
            <div>
              <img
                className={classes.image}
                alt="Presentation"
                src="/img/dark-light.png"
              />
            </div>
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
}

export default ThirdHero;