import {
  Card,
  CardHeader,
  CardContent,
  CardActions,
  Grid,
  Box,
  IconButton,
  Avatar,
  Typography,
  makeStyles,
} from "@material-ui/core";
import clsx from "clsx";
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';
import LinkedInIcon from '@material-ui/icons/LinkedIn';

const styles = (theme) => ({
  avatar: {
    height: theme.spacing(8),
    width: theme.spacing(8)
  },
  logo: {
    height: theme.spacing(5),
    width: theme.spacing(10)
  },
  card: {
    maxWidth: 330,
    height: 350
  }
});

const useStyles = makeStyles(styles);

function TeamCard({className=0, avatarSrc, name, title, logoSrc, description}){
  const classes = useStyles();
  return (
    <Card className={clsx(classes.card, className)}>
      <CardContent>
        <Grid container direction="column" alignItems="center" spacing={1}>
          <Grid item>
            <Avatar 
              aria-label="profile-picture" 
              src={avatarSrc}
              alt="profile-pic"
              className={classes.avatar}
            />
          </Grid>
          <Grid item>
            <Typography variant="h4">
              {name}
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="h5">
              {title}
            </Typography>
          </Grid>
          <Grid item>
            <img src={logoSrc} className={classes.logo}></img>
          </Grid>
          <Grid item>
            <Typography variant="body2" paragraph>
              {description}
            </Typography>
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
}

export default TeamCard;