import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  makeStyles,
  IconButton,
  Button,
  Typography
} from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";

const styles = (theme) => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
  dialogActions: {
    paddingLeft: theme.spacing(3),
    paddingRight: theme.spacing(3)
  }
});

const useStyles = makeStyles(styles);

function ConfirmationModal({ open, handleClose, title, text, handleConfirm }) {
  const classes = useStyles();
  
  return (
    <Dialog
      open={open}
      onClose={handleClose}
      aria-labelledby="are-you-sure-dialog-title"
      aria-describedby="are-you-sure-dialog-description"
      maxWidth="xs"
      fullWidth
    >
      <DialogTitle disableTypography>
        <Typography variant="h4">
          {title}
        </Typography>
        <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent dividers>
        <Typography>
          {text}
        </Typography>
      </DialogContent>
      <DialogActions className={classes.dialogActions}>
        <Button 
          variant="outlined" 
          onClick={handleClose}  
          color="primary"
        >
          {"Cancel"}
        </Button>
        <Button 
            variant="contained" 
            onClick={handleConfirm}  
            color="primary"
          >
          {"Confirm"}
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default ConfirmationModal;