import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Box,
  makeStyles,
  IconButton,
  InputAdornment
} from "@material-ui/core";
import {
  Alert
} from "@material-ui/lab";
import CloseIcon from '@material-ui/icons/Close';
import EmailIcon from '@material-ui/icons/Email';
import { sendResetEmail } from "../firebase/auth";

const styles = (theme) => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  }
});

const useStyles = makeStyles(styles);

function ForgotPasswordModal({ open=false, handleClose }) {
  const classes = useStyles();
  const [values, setValues] = React.useState({
    email: ""
  });
  const [alertMessage, setAlertMessage] = React.useState({
    open: false,
    severity: "",
    message: ""
  })
  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };
  const handleSendResetEmail = async () => {
    try {
      await sendResetEmail(values.email);
      setAlertMessage({
        open: true,
        severity: "success",
        message: "You should soon receive an email allowing you to reset your password. Please make sure to check your spam and trash if you can't find the email."
      })
    } catch(error) {
      setAlertMessage({
        open: true,
        severity: "error",
        message: error.message
      })
    }
  };
  const handlePressEnterKey = async (event) => {
    if(event.key == "Enter"){
      await handleSendResetEmail()
    }
  };
  return (
    <Dialog maxWidth="xs" open={open} onClose={handleClose} aria-labelledby="password-reset-dialog" fullWidth>
      <DialogTitle>
        Forgot Your Password?
        <IconButton aria-label="close" className={classes.closeButton} onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent dividers>
        {alertMessage.open &&
          <Box>
            <Alert severity={alertMessage.severity}>
              {alertMessage.message}
            </Alert>
          </Box>
        }
        <TextField
          autoFocus
          margin="normal"
          id="forgotEmailText"
          label="Forgot Email Address"
          type="email"
          value={values.email}
          onChange={handleChange('email')}
          variant="outlined"
          fullWidth
          placeholder="Email"
          onKeyPress={handlePressEnterKey}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <EmailIcon />
              </InputAdornment>
            ),
          }}
        />
      </DialogContent>
      <DialogActions>
        <Button 
          fullWidth 
          variant="contained" 
          onClick={handleSendResetEmail} 
          fullWidth 
          color="primary"
        >
          Reset Password
        </Button>
      </DialogActions>
    </Dialog>
  )
}

export default ForgotPasswordModal;