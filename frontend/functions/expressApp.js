const express = require('express');
const cors = require('cors');
const { NodeVM, VMScript, } = require('vm2');
const assert = require('assert').strict;
const execSync = require('child_process').execSync;
const util = require('util');
const exec = util.promisify(require('child_process').exec);
const writeFile = util.promisify(require('fs').writeFile);
const deleteFile = util.promisify(require('fs').unlink);
const fsExtra = require('fs-extra');
const clearModule = require('clear-module');
const os = require("os");
const deepEqual = require('deep-equal');
const consoleNewLog = require("./consoleNewLog");

const app = express();

// Automatically allow cross-origin requests
app.use(cors({ origin: true }));
app.use(express.json());

const officialTwoSum = function (nums, target) {
  const hashMap = {};
  
  for (let i = 0; i < nums.length; i++) {
    const element = target - nums[i];
    if (nums[i] in hashMap) {
      return [hashMap[nums[i]], i];
    }
    hashMap[element] = i;
  }
  return [];
}

app.post('/', async (req, res) => {
  const { language, codeInput, testCases, functionTitle } = req.body;
  let consoleOutputArr = [];
  const sandbox = {
    console: {
      log: function(...args) { consoleOutputArr.push(consoleNewLog(...args))}
    }
  }
  const nodeVm = new NodeVM({
    console: 'redirect',
    sandbox: sandbox,
    require: {
      builtin: ['fs', 'path'],
      root: "./",
      mock: {
          fs: {
              readFileSync() { return 'Nice try!'; }
          },
          process: {
            cwd() { return 'Nice Try!'; }
          }
      }
    }
  });
  codeInputWithExport = codeInput + "\n" + `module.exports = ${functionTitle};`;
  const script = new VMScript(codeInputWithExport, `solution.js`);
  let i;
  let userAns;
  let officialAns;
  for(let i = 0; i < testCases[language].length; i++){
    try {
      const testCase = testCases[language][i];
      const userTypedFunction = nodeVm.run(script);
      const officialFunction = officialTwoSum;
      const userAns = userTypedFunction(...testCase.arguments);
      const officialAns = officialFunction(...testCase.arguments);
      // assert.deepStrictEqual(userAns, officialAns);
      const result = deepEqual(userAns, officialAns);
      if(!result){
        return res.status(200).json({
          status: "assertError",
          lastWrongInput: testCases[language][i].arguments ? 
                          testCases[language][i].arguments.map(JSON.stringify): 
                          testCases[language][i].arguments,
          lastWrongOutput: JSON.stringify(userAns),
          lastWrongExpectedOutput: JSON.stringify(officialAns),
          stdout: consoleOutputArr.join("")
        })
      }
      consoleOutputArr = [];
    } catch(error) {
        console.log("runTimeError", error);
        return res.status(200).json({
          status: "runTimeError",
          stderr: error.stack
        })
    }
  }
  return res.status(200).json({
    status: "passed"
  })
});
// const assertUserFunction = (officialFunction, userTypedFunction, testCases) => {
//   let i;
//   let userAns;
//   let officialAns;
//   console.log("testCases", testCases[0]);
//   console.log("userTypedFunction", userTypedFunction.toString());
//   try {
//     for (i = 0; i < testCases.length; i++) {
//       userAns = userTypedFunction(...testCases[i].arguments);
//       console.log("userAns", userAns);
//       officialAns = officialFunction(...testCases[i].arguments);
//       console.log("officalAns", officialAns);      
//       assert.deepStrictEqual(userAns, officialAns);
//     }
//   } catch(error) {
//     console.log("assert", error);
//     return {
//       status: "assertError",
//       lastWrongInput: testCases[i].arguments ? testCases[i].arguments.map(JSON.stringify): testCases[i].arguments,
//       lastWrongUserOutput: JSON.stringify(userAns),
//       lastWrongExpectedOutput: JSON.stringify(officialAns)
//     }
//   }
//   return {
//     status: "passed"
//   }
// }

// const execUserCodeInFile = async (filePath) => {
//   let output;
//   try {
//     output = await exec(`node ${filePath}`);
//   } catch(error) {
//     const replace = `/.*${os.tmpdir()}`;
//     const regex = new RegExp(replace, "g");
//     const stderrStr = error.stderr.toString();
//     const stderrRegexed = stderrStr.replace(regex, "");
//     return {
//       status: "runTimeError",
//       stderr: stderrRegexed
//     }
//   }
//   return {
//     status: "passed",
//     stdout: output.stdout.toString()
//   }
// }

// app.post('/', async (req, res) => {
//   try {
//     const { language, codeInput, testCases, functionTitle } = req.body;
//     console.log(language, testCases);
//     const codeInputExec = testCases[language].reduce((acc, curr) => {
//       return acc + '\n' + curr.codeInput
//     }, codeInput) + '\n' + `module.exports = ${functionTitle};`;
//     console.log(codeInputExec);
//     const filePath = `${os.tmpdir()}/solution.js`
//     // console.log("yo1", fs.readdirSync(os.tmpdir()).toString());
//     await writeFile(filePath, codeInputExec);
//     const execStatusObject = await execUserCodeInFile(filePath);
//     if(execStatusObject.status === "runTimeError") {
//       res.status(200).json(execStatusObject);
//       return;
//     } 
//     // console.log("content", fs.readFileSync(filePath).toString());
//     clearModule(filePath);
//     const userTypedFunction = require(filePath);
//     // console.log("yo", fs.readdirSync(os.tmpdir()).toString());
//     // console.log("userTypedFunction1", userTypedFunction.toString());
//     const assertStatusObject = assertUserFunction(officialTwoSum, userTypedFunction, testCases[language]);
//     res.status(200).json({...assertStatusObject, stdout: execStatusObject.stdout});
//     deleteFile(filePath);
//     return;
//   } catch(error) {
//     console.log("outerError", error);
//     res.status(500).json({
//       message: "Something Went Wrong"
//     })
//   }
// });

// app.post('/', (req, res) => {
//   let status = null;
//   let codeInput;
//   let language;
//   let stdout;
//   let stderror = "";
//   let userAns;
//   let officialAns;
//   try {
//     language = req.body.language;
//     codeInput = req.body.codeInput;
//     testCases = req.body.testCases;
//     functionTitle = req.body.functionTitle;
//     solution = req.body.solution;
//     console.log(JSON.stringify(req.body));
//     console.log(language, codeInput, testCases, functionTitle);
//     const codeInputExec = testCases[language].reduce((acc, curr) => {
//       return acc + '\n' + curr
//     }, codeInput) + '\n' + `module.exports = ${functionTitle};`;

//     // const codeInputExec = codeInput + '\n' + testCases[language][0].codeInput + '\n' +
//     //                       `module.exports = ${functionTitle};`;
//     console.log("codeInputExec", codeInputExec);
//     fs.writeFileSync(`${os.tmpdir()}/solution.js`, codeInputExec);
//     try {
//       stdout = execSync(`node ${os.tmpdir()}/solution.js`).toString();
//       console.log("stdput", stdout);
//       const userTypedFunction = require(`${os.tmpdir()}/solution.js`);
//       try {
//         for (const testCase in testCases[language]) {
//           userAns = userTypedFunction(...testCase.arguments);
//           const officialFunction = twoSum;
//           officialAns = officialFunction(...testCase.arguments);
//           console.log("official ans", officialAns);
//           console.log("user typed function", userTypedFunction);
//           console.log("userAns", userAns);
//           assert.deepStrictEqual(officialAns, userAns);
//         }
//       } catch(error) {
//         console.log("assert", error);
//       }
//     } catch(error) {
//       console.log(typeof(error));
//       console.log("error.status", error.status);
//       console.log("error.message", error.message);
//       console.log("error.stderr", error.stderr.toString());
//       console.log("error.stdout", error.stdout.toString());
//       const replace = `/.*${os.tmpdir()}`;
//       console.log("replace", replace);
//       const regex = new RegExp(replace, "g");
//       stderror = error.stderr.toString();
//       stderror = stderror.replace(regex, "");
//       status = "runTimeError";
//     }
//   } catch(error) {
//     console.log(error);
//     res.status(500).json({
//       error: error,
//       message: "Something Went Wrong"
//     })
//     return;
//   }
//   res.status(200).json({
//     status,
//     result: {
//       stderror,
//       stdout,
//       lastWrongInput,
//       lastWrongYourOutput,
//       expectedOutput
//     }
//   })
// });

module.exports = app;