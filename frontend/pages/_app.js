import React from 'react';
import PropTypes from 'prop-types';
import Head from 'next/head';
import { ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from '../components/ui/Theme';
import 'codemirror/lib/codemirror.css';
import 'codemirror/theme/material.css';
import 'codemirror/theme/zenburn.css';
import "react-splitter-layout/lib/index.css";
import 'react-reflex/styles.css';
import '../components/ui/App.css';
import Router from 'next/router';
import { 
  Backdrop, 
  CircularProgress 
} from '@material-ui/core';
import UserProvider from "../components/providers/UserProvider";

export default function MyApp(props) {
  const { Component, pageProps } = props;
  const [isRedirecting, setRedirecting] = React.useState(false);

  React.useEffect(() => {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }, []);

  React.useEffect(() => {
    Router.events.on('routeChangeStart', () => {
      setRedirecting(true);
    });

    Router.events.on('routeChangeComplete', () => {
      setRedirecting(false);
    });

    Router.events.on('routeChangeError', () => {
      setRedirecting(false);
    });
  }, []);

  return (
    <React.Fragment>
      <Head>
        <title>My page</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
      <ThemeProvider theme={theme}>
        <UserProvider>
          {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
          <CssBaseline />
          {isRedirecting ?
          <Backdrop open={isRedirecting}>
            <CircularProgress />
          </Backdrop> :
          <Component {...pageProps} />}
        </UserProvider>
      </ThemeProvider>
    </React.Fragment>
  );
}

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired,
};