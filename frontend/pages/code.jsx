import CodingPlayground from "../components/CodingPlayground";
import Problem from "../components/Problem"
import * as problems from "../mockData/problems";
import {
  Grid,
  Box,
  makeStyles
} from "@material-ui/core"
import dynamic from 'next/dynamic';
// const SplitterLayout = dynamic(() => {
//   return import("react-splitter-layout");
// }, { ssr: false });
import CompactHeader from "../components/CompactHeader";
import Header from "../components/Header";
import {
  ReflexContainer,
  ReflexSplitter,
  ReflexElement
} from 'react-reflex';

const styles = (theme) => ({
  splitter: {
    width: "5px"
  }
});

const useStyle = makeStyles(styles);

function CodeEditorPage() {
  const classes = useStyle();
  const [maxSize, setMaxSize] = React.useState(false);

  const handleResize = () => {
    setMaxSize(!maxSize);
  }

  return (
    <Box height={"100vh"}> 
      <ReflexContainer orientation="horizontal">
        <ReflexElement className="header" flex={0.05}>
          <CompactHeader />
        </ReflexElement>
        <ReflexElement>
          <ReflexContainer orientation="vertical">
            {!maxSize && 
            <ReflexElement className="left-pane">
              <Problem problem={problems.reorderDataInLogFiles}/>
            </ReflexElement>}
            <ReflexSplitter className={classes.splitter}/>
            <ReflexElement className="right-pane">
              <CodingPlayground problem={problems.reorderDataInLogFiles} handleResize={handleResize} maxSize={maxSize}/>
            </ReflexElement>
          </ReflexContainer>
        </ReflexElement>
      </ReflexContainer>
    </Box>
  );
};

export default CodeEditorPage;