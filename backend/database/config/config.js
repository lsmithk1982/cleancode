
const config = {
  development: {
    username: 'mohamedisse',
    password: 'myPassword',
    database: 'dev_db',
    host: '127.0.0.1',
    dialect: 'postgres',
  },
  test: {
    username: 'mohamedisse',
    password: 'myPassword',
    database: 'test_db',
    host: '127.0.0.1',
    dialect: 'postgres',
  },
  production: {
    username: 'root',
    password: null,
    database: 'database_test',
    host: '127.0.0.1',
    dialect: 'postgres',
  },
};

module.exports = config;
