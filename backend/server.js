const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const authRoutes = require('./src/routes/auth');

require('dotenv').config();

const app = express();

app.use(morgan('dev'));
app.use(bodyParser.json());

// routes
app.use('/api', authRoutes);

const port = process.env.PORT || 8000;
app.listen(port, () => {
  console.log(`listening on ${port}`);
});
