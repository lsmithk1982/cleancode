/* eslint-disable */
const { check, validationResult } = require('express-validator');
const expressJwt = require('express-jwt');
const models = require('../../database/models');

const isUserNameNotRegistered = async (value) => {
  try {
    console.log('Username value', value);
    const user = await models.User.findAll({
      where: {
        name: value,
      },
    });

    if (!user) {
      return Promise.reject();
    }
  } catch (err) {
    console.log('err', err);
  }
};

const isEmailNotRegistered = async (value) => {
  try {
    console.log('email value', value);
    const user = await models.User.findAll({
      where: {
        email: value,
      },
    });

    if (!user) {
      return Promise.reject();
    }
  } catch (err) {
    console.log('err', err);
  }
};

exports.userSignupValidator = [
  check('username').not().isEmpty().withMessage('Username is required')
    .isLength({ min: 5, max: 10 })
    .withMessage('Username has to be between 5-10 characters long')
    .isAlphanumeric()
    .withMessage('Username can only contain letters and numbers')
    .custom(isUserNameNotRegistered)
    .withMessage('User name is already registered'),
  check('password').not().isEmpty().withMessage('Password is required')
    .isLength({ min: 6, max: 20 })
    .withMessage('Password has to be between 6-20 characters long')
    .not()
    .isAlphanumeric()
    .withMessage('Password must contains at least one special characters'),
  check('passwordConf').not().isEmpty().withMessage('Password Confirmation is required')
    .custom((value, { req }) => {
      if (value !== req.body.password) {
        return false;
      }
      return true;
    })
    .withMessage('Password confirmation is incorrect'),
  check('email').not().isEmpty().withMessage('Email is required')
    .isEmail()
    .withMessage('Please enter is valid email address')
    .custom(isEmailNotRegistered)
    .withMessage('Email is already registered'),
];

exports.runValidation = async (req, res, next) => {
  const errors = validationResult(req);
  console.log('errors', errors.array());
  if (!errors.isEmpty()) {
    return res.status(422).json({ error: errors.array()[0].msg });
  }
  next();
};

exports.userSignInValidator = [
  check('username').not().isEmpty().withMessage('Username is required'),
  check('email').not().isEmpty().withMessage('Email is required'),
  check('password').not().isEmpty().withMessage('Password is required')
    .isLength({ min: 6, max: 20 })
    .withMessage('Password has to be between 6-20 characters long')
    .not()
    .isAlphanumeric()
    .withMessage('Password must contains at least one special characters'),
];

exports.isJwtValid = expressJwt({
  secret: process.env.JWT_SECRET,
});
